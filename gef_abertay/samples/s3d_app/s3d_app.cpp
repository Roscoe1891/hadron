#include "s3d_app.h"

#include <graphics/font.h>
#include <graphics/renderer_3d.h>
#include <graphics/scene.h>
#include <graphics/sprite_renderer.h>

#include <input/touch_input_manager.h>
#include <input/sony_controller_input_manager.h>
#include <maths/vector2.h>
#include <maths/math_utils.h>
#include <platform/d3d11/system/platform_d3d11.h>
#include <system/platform.h>


S3DApp::S3DApp(gef::Platform& platform) :
	Application(platform),
	sprite_renderer_(NULL),
	font_(NULL),
	input_manager_(NULL)
{
}

void S3DApp::Init()
{
	// Create sprite renderer
	sprite_renderer_ = platform_.CreateSpriteRenderer();

	// Initialise Font
	InitFont();

	// Initialise S3D Renderer
	s3d_renderer_.Init(platform_);

	// Initialise camera
	camera_.Init();

	// Set up lights
	SetupLights();

	// set up input manager
	input_manager_ = input_manager_->Create(platform_);

	// init state manager
	state_manager_.Init(&platform_, &s3d_renderer_, &camera_, &game_data_, &mesh_data_manager_);

}

void S3DApp::CleanUp()
{
	// Clean up font
	CleanUpFont();

	// Clean up s3d renderer
	s3d_renderer_.CleanUp();

	// free up sprite renderer
	delete sprite_renderer_;
	sprite_renderer_ = NULL;
}

bool S3DApp::Update(float frame_time)
{
	// calc fps
	fps_ = 1.0f / frame_time;

	bool running = true;
	//if( ::GetAsyncKeyState(VK_ESCAPE) & 0x8000f)
		//running = false;

	// update input
	input_manager_->Update();

	// update current state
	if (!state_manager_.Update(input_manager_, frame_time))
		running = false;

	return running;
}

void S3DApp::Render()
{
	// Draw 3d Scene
	s3d_renderer_.Render3DScene(platform_, sprite_renderer_, &camera_, state_manager_.GetCurrentState());

	// Draw UI
	sprite_renderer_->Begin(false);	
	DrawHUD();

	// Draw current state UI
	state_manager_.RenderUI(sprite_renderer_, font_);
	sprite_renderer_->End();
}

void S3DApp::InitFont()
{
	// Create and load new font
	font_ = new gef::Font(platform_);
	font_->Load("montserrat");
}

void S3DApp::CleanUpFont()
{
	delete font_;
	font_ = NULL;
}

void S3DApp::DrawHUD()
{
	if(font_)
	{
		// Debug: Draw FPS
		//font_->RenderText(sprite_renderer_, gef::Vector4(780.0f, 500.0f, -0.9f), 1.f, 0xffffffff, gef::TJ_LEFT, "FPS: %.1f", fps_);

		// Debug: Draw convergence distance
		//font_->RenderText(sprite_renderer_, gef::Vector4(780.0f, 400.0f, -0.9f), 1.f, 0xffffffff, gef::TJ_LEFT, "CD: %.2f", s3d_renderer_.convergence_distance());

		// Debug: Eye Seperation Distance
		//font->RenderText(sprite_renderer, gef::Vector4(780.0f, 300.0f, -0.9f), 1.f, 0xffffffff, gef::TJ_LEFT, "ES: %.3f", s3d_renderer_->eye_seperation_distance());
	}
}

void S3DApp::SetupLights()
{
	// declare and define a point light
	gef::PointLight default_point_light;
	default_point_light.set_colour(gef::Colour(0.7f, 0.7f, 1.0f, 1.0f));
	//DEFAULT POS: default_point_light.set_position(gef::Vector4(-500.0f, 400.0f, 700.0f));
	default_point_light.set_position(gef::Vector4(0.0f, 0.0f, 70.0f));

	// set shader data re point light 
	gef::Default3DShaderData& default_shader_data = s3d_renderer_.renderer_3d()->default_shader_data();
	default_shader_data.set_ambient_light_colour(gef::Colour(0.5f, 0.5f, 0.5f, 1.0f));
	default_shader_data.AddPointLight(default_point_light);
}



