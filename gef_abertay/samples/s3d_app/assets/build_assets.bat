rem build assets from fbx to scn files

set FBX2SCN_EXE=..\..\..\tools\fbx2scn\bin\win_x64\fbx2scn.exe
set OUTPUT_DIR=..\..\media
 
rem Repeat the following lines if you have more than one asset
 
rem change asset_name to the name of you FBX file without the .FBX extension

rem ASSET 1
set ASSET_NAME=sphere
 
set FBX2SCN_FLAGS= -ignore-skinning -strip-texture-path -texture-extension .png
%FBX2SCN_EXE% -o %OUTPUT_DIR%\%ASSET_NAME%.scn %FBX2SCN_FLAGS% %ASSET_NAME%.fbx
 

rem ASSET 2
set ASSET_NAME=cylinder_8
 
set FBX2SCN_FLAGS= -ignore-skinning -strip-texture-path -texture-extension .png
%FBX2SCN_EXE% -o %OUTPUT_DIR%\%ASSET_NAME%.scn %FBX2SCN_FLAGS% %ASSET_NAME%.fbx



rem ASSET 3
set ASSET_NAME=cylinder_12
 
set FBX2SCN_FLAGS= -ignore-skinning -strip-texture-path -texture-extension .png
%FBX2SCN_EXE% -o %OUTPUT_DIR%\%ASSET_NAME%.scn %FBX2SCN_FLAGS% %ASSET_NAME%.fbx
