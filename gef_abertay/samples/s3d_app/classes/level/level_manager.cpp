#include "level_manager.h"

namespace s3d_app
{
	LevelManager::LevelManager()
	{
		AssignLevelData();
	}

	LevelManager::~LevelManager()
	{
	}

	void LevelManager::InitLevel(MeshDataManager* mesh_data_manager, int level_number)
	{
		// check level number
		switch (level_number)
		{
			// init using corresponding level data
		case 0:
			current_level_.Init(mesh_data_manager->scene_meshes()[MeshDataManager::LEVEL1_MESH], level_data_[level_number]);
			break;
		case 1:
			current_level_.Init(mesh_data_manager->scene_meshes()[MeshDataManager::LEVEL2_MESH], level_data_[level_number]);
			break;
		default:
			break;
		}
	}

	void LevelManager::AssignLevelData()
	{
		// populate level data
		
		// for first level
		level_data_[0].bottom_face = 6;
		level_data_[0].enemies_this_level = 30;
		level_data_[0].level_faces = 8;
		level_data_[0].level_number = 0;

		// for second level
		level_data_[1].bottom_face = 9;
		level_data_[1].enemies_this_level = 40;
		level_data_[1].level_faces = 12;
		level_data_[1].level_number = 1;
	}

	bool LevelManager::OnLastLevel()
	{
		// return true if current level is the last level
		if (current_level_.level_number() == MAX_LEVELS - 1)
			return true;
		else
			return false;
	}
}