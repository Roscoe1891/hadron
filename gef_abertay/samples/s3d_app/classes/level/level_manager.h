#ifndef _LEVEL_MANAGER_H
#define _LEVEL_MANAGER_H

#include "level.h"

// Level Manager stores data for all levels and initialises new levels

namespace s3d_app
{
#define MAX_LEVELS 2

	class LevelManager
	{
	public:
		LevelManager();
		~LevelManager();

		// get the current level being played
		inline Level& current_level() { return current_level_; }

		// Initialise the level using the data @ index "level number"
		void InitLevel(MeshDataManager* mesh_data_manager, int level_number);

		// returns true if current level is last level
		bool OnLastLevel();

	private:
		// assign level data to level data structs array
		void AssignLevelData();
		
		// Level Data for all levels
		Level::LevelData level_data_[MAX_LEVELS];

		// the current level
		Level current_level_;
	};
}

#endif