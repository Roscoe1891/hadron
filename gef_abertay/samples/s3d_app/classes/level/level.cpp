#include "level.h"

namespace s3d_app
{
	Level::Level()
	{
		level_faces_ = 0;
		enemies_this_level_ = 0;
	}

	Level::~Level()
	{
	}

	void Level::Init(gef::Mesh* mesh, LevelData level_data)
	{
		// assign from level data
		level_number_ = level_data.level_number;
		level_faces_ = level_data.level_faces;
		bottom_face_ = level_data.bottom_face;
		enemies_this_level_ = level_data.enemies_this_level;
		
		//level_mesh_.set_mesh(mesh_data_manager->scene_meshes()[MeshDataManager::LEVEL_MESH]);
		level_mesh_.set_mesh(mesh);

		// level geometry will always be circular for now
		level_geometry_type_ = CIRCULAR;

	}

	void Level::Update(float dt)
	{
	}

	void Level::Render(gef::Renderer3D* renderer_3d)
	{
		// draw the level
		renderer_3d->DrawMesh(level_mesh_);
	}

	void Level::CleanUp()
	{
	}

}