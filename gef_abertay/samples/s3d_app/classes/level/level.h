#ifndef _LEVEL_H
#define _LEVEL_H

#include <assets/obj_loader.h>
#include <classes/graphics/mesh_data_manager.h>
#include <graphics/mesh_instance.h>
#include <graphics/model.h>
#include <graphics/renderer_3d.h>

// Geometry and data that constitues a level in the game world

namespace s3d_app
{
	class Enemy;

	class Level
	{
	public:
		// Initialisation data
		struct LevelData
		{
			int level_number, level_faces, bottom_face, enemies_this_level;
		};

		// The levels geometry type
		enum LevelGeometryType { CIRCULAR };

		Level();
		~Level();
		// Initialise level
		void Init(gef::Mesh* mesh, LevelData level_data);
		// Update level
		void Update(float dt);
		// Render level
		void Render(gef::Renderer3D* renderer_3d);
		// Clean up
		void CleanUp();

		// get number of faces this level has
		inline int level_faces() const { return level_faces_; }
		// get the number of the face which is flat to the bottom of the screen
		inline int bottom_face() const { return bottom_face_; }
		// get the level number
		inline int level_number() const { return level_number_; }

		// get the level's geometry type
		inline LevelGeometryType level_geometry_type() const { return level_geometry_type_; }
		// get the number of enemies which need to be destroyed to clear this level
		inline int enemies_this_level() const { return enemies_this_level_; }

	private:
		// the mesh that forms this level's geometry
		gef::MeshInstance level_mesh_;				

		// the type of this level's geometry
		LevelGeometryType level_geometry_type_;	
		// this level's number
		int level_number_;	
		// the number of faces the level's geometry has
		int level_faces_;	
		// the face on this level which is flat to the bottom of the screen
		int bottom_face_;		
		// the number of enemies that need to be destroyed to clear this level
		int enemies_this_level_;					
	};
}

#endif // !_LEVEL_H
