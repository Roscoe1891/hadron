#ifndef _TEXT_H
#define _TEXT_H

#include <string>
#include <graphics\font.h>
#include "gui.h"

using std::string;

/// As a child of GUI Text is positioned based on an 'anchor' and 'offset' (see GUI base class).
/// It should be used to display a string as (part of) a UI element. 

namespace s3d_app
{
	class Text : public GUI
	{
	public:
		Text();
		~Text();
		void InitText(gef::Vector4 anchor_pos, float x_off, float y_off, string text, float scale, UInt32 colour, gef::TextJustification justify);

		// From base class
		virtual void Update(gef::Vector4 camera_pos);
		virtual void Render(gef::SpriteRenderer* sr, gef::Font* font);
		virtual void AlignToPosition(gef::Vector4 pos);

		// get text
		inline const string& text() { return text_; }
		// set text
		inline void set_text(string text) { text_ = text; }

		// get the colour of the text (ABGR)
		inline const UInt32 colour() const { return colour_; }
		// set the colour of the text (ABGR)
		inline void set_colour(const UInt32 colour) { colour_ = colour; }

		// set text from an int value
		void set_text_from_int(int text);

	private:
		// the text
		string text_;		
		// Text's colour
		UInt32 colour_;					
		// Text's justification
		gef::TextJustification justification_;	
		// Text's scale
		float scale_;								
	};
}

#endif
