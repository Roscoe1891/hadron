#include "gui.h"

namespace s3d_app
{
	GUI::GUI() : k_z_position_(-0.9f)
	{
	}

	GUI::~GUI()
	{
	}

	void GUI::Init(gef::Vector4 anchor_pos, float x_off, float y_off)
	{
		// set offsets
		x_offset_ = x_off;
		y_offset_ = y_off;
		// set position
		(set_position(anchor_pos.x() + x_offset_, anchor_pos.y() + y_offset_, k_z_position_));
	}

	void GUI::Update(gef::Vector4 camera_pos)
	{}

	void GUI::Render(gef::SpriteRenderer* sr, gef::Font* font)
	{}

	void GUI::AlignToPosition(gef::Vector4 pos)
	{
		// Can be overridden by derived classes
	}

	std::string GUI::ToString(int value)
	{
		// return provided int value as a string

		// create an output string stream
		std::ostringstream os;

		// throw the value into the string stream
		os << value;

		// convert the string stream into a string and return
		return os.str();
	}
}