#ifndef _TEXT_MENU_H
#define _TEXT_MENU_H

#include "gui.h"
#include "text.h"

/// Text menu contains an array of text items which can be used to implement a menu

namespace s3d_app
{
#define MAX_MENU_ITEMS 5

	class TextMenu : public GUI
	{
	public:
		TextMenu();
		~TextMenu();

		// From Base Class
		virtual void Update(gef::Vector4 camera_pos);
		virtual void Render(gef::SpriteRenderer* sr, gef::Font* font);
		virtual void AlignToPosition(gef::Vector4 pos);

		// get selected
		inline const int& selected() const { return selected_; }
		
		// Text Menu specific initialisation
		void InitTextMenu(gef::Vector4 anchor_pos, float x_off, float y_off,
			float scale, float text_spacing, UInt32 default_colour, UInt32 select_colour, gef::TextJustification justify);

		// Param in a string to be added as a menu item
		void AddMenuItem(string new_item);
		// Param in a string to replace the current text of the selected item
		void SetSelectedText(string new_text);
		// Param in an int to replace the current text of the selected item
		void SetSelectedText(int new_text);
		// Param in new value of selected
		void SetSelected(int new_selection);

	private:
		// array holding the menu text items
		Text menu_items_[MAX_MENU_ITEMS];
		// index of currently selected item
		int selected_;
		// number of menu slots in use
		int active_menu_items_;						

		// Scale for menu font
		float menu_font_scale_;	
		// Vertical spacing between menu items
		float menu_text_spacing_;	
		// Default colour for menu text
		UInt32 default_text_colour_;
		// Colour of selected menu item
		UInt32 selected_text_colour_;	
		// Justification of menu text
		gef::TextJustification menu_text_justify_;	

	};
}

#endif