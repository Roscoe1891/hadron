#include "counter.h"

namespace s3d_app
{
	Counter::Counter()
	{
		count_ = 0;
	}

	Counter::~Counter()
	{
	}

	void Counter::InitCounter(gef::Vector4 anchor_pos, float x_off, float y_off, string title_text, float scale, UInt32 colour, gef::TextJustification justify)
	{
		Init(anchor_pos, x_off, y_off);
		
		fixed_text_ = title_text;

		// Initialise counter text 
		text_.InitText(anchor_pos, x_offset_, y_offset_, title_text, scale, colour, justify);
		// Force update text
		UpdateText();
	}

	void Counter::Update(gef::Vector4 camera_pos)
	{
		// Align to camera position
		AlignToPosition(camera_pos);
	}

	void Counter::UpdateCounter(float count)
	{
		// Check value of count and update if changed
		if (count_ != count)
		{
			count_ = count;
			UpdateText();
		}
	}

	void Counter::UpdateText()
	{
		// set text to show value of count
		string c = ToString((int)count_);
		text_.set_text(fixed_text_ + c);
	}

	void Counter::AlignToPosition(gef::Vector4 camera_pos)
	{
		// align counter to provided position, relative to its offset values
		position_ = gef::Vector4(camera_pos.x() + x_offset(), camera_pos.y() + y_offset(), k_z_position_);
		text_.AlignToPosition(position());
	}

	void Counter::SetColour(const UInt32 colour)
	{ 
		text_.set_colour(colour); 
	}

	void Counter::Render(gef::SpriteRenderer* sr, gef::Font* font)
	{
		// Draw counter text
		text_.Render(sr, font);
	}

}