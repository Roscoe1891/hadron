#ifndef _GUI_H
#define _GUI_H

#include <iostream>
#include <string>
#include <sstream>
#include <graphics/sprite_renderer.h>
#include <graphics/font.h>
#include <maths/vector4.h>

/// GUI is the base class for all UI elements.
/// GUI's are positioned in relation to a notional 'anchor', which is normally the camera.
/// The Init Method takes the x and y position of the anchor along with the x and y offsets.
/// The offsets are then used to adjust the GUI's position in relation to the anchor.
/// GUI has no implicit visual representation in the game world.
/// UI elements inheriting from GUI should act as containers for objects such as text or sprites to represent the GUI.

namespace s3d_app
{

	class GUI
	{
	public:
		GUI();
		virtual ~GUI();
		// Update GUI object
		virtual void Update(gef::Vector4 camera_pos) = 0;
		// Render GUI object
		virtual void Render(gef::SpriteRenderer* sr, gef::Font* font) = 0;

		//param[in] position object is to be aligned to via its offset. 
		// For example the camera or containing object. 
		virtual void AlignToPosition(gef::Vector4 pos) = 0;

		// intialise GUI
		void Init(gef::Vector4 anchor_pos, float x_off, float y_off);

		// set position
		inline void set_position(const gef::Vector4& position) { position_ = position; }
		inline void set_position(const float x, const float y, const float z) { position_.set_x(x); position_.set_y(y); position_.set_z(z); }
		// get position
		inline const gef::Vector4& position() const { return position_; }
		//set x_offset
		inline void set_x_offset(float x_off) { x_offset_ = x_off; }
		// get x_offset
		inline const float& x_offset() { return x_offset_; }
		// set y_offset
		inline void set_y_offset(float y_off) { y_offset_ = y_off; }
		// get y_offset
		inline const float& y_offset() { return y_offset_; }
		

	protected:
		// position on screen (z value used for depth sorting sprites)
		gef::Vector4 position_;			
		// horizontal offset
		float x_offset_;
		// vertical offset
		float y_offset_;
		// depth offset
		float k_z_position_;
		// Conversion: int to string
		std::string ToString(int value);		

	};
}

#endif