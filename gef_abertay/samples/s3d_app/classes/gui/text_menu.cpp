#include "text_menu.h"

namespace s3d_app
{
	TextMenu::TextMenu()
	{
		active_menu_items_ = 0;
		selected_ = 0;
	}

	TextMenu::~TextMenu()
	{
	}

	void TextMenu::Update(gef::Vector4 camera_pos)
	{}
	
	void TextMenu::Render(gef::SpriteRenderer* sr, gef::Font* font)
	{
		// render all text items
		for (int i = 0; i < active_menu_items_; i++)
		{
			menu_items_[i].Render(sr, font);
		}
	}

	void TextMenu::AlignToPosition(gef::Vector4 pos)
	{
		// update position and apply offset
		position_.set_x(pos.x() + x_offset_);
		position_.set_y(pos.y() + y_offset_);
		position_.set_z(k_z_position_);

		// realign menu items
		for (int i = 0; i < active_menu_items_; i++)
		{
			menu_items_[i].AlignToPosition(position_);
		}
	}

	void TextMenu::InitTextMenu(gef::Vector4 anchor_pos, float x_off, float y_off,
		float scale, float text_spacing, UInt32 default_colour, UInt32 select_colour, gef::TextJustification justify)
	{
		// Base init
		Init(anchor_pos, x_off, y_off);
		
		// Assign values
		menu_font_scale_ = scale;
		menu_text_spacing_ = text_spacing;
		default_text_colour_ = default_colour;
		selected_text_colour_ = select_colour;
		menu_text_justify_ = justify;
		active_menu_items_ = 0;
	}

	void TextMenu::AddMenuItem(string new_item)
	{
		// Initilaise new item
		menu_items_[active_menu_items_].InitText(position_, 0.f, active_menu_items_ * menu_text_spacing_,
			new_item, menu_font_scale_, default_text_colour_, menu_text_justify_);
		// Align new item
		menu_items_[active_menu_items_].AlignToPosition(position_);
		// Increment active items
		active_menu_items_++;
	}

	void TextMenu::SetSelected(int new_selected)
	{
		// change current selected colour to default
		menu_items_[selected_].set_colour(default_text_colour_);

		// set new selected (accounting for wraparound)
		if (new_selected < 0)
		{
			selected_ = active_menu_items_ - 1;
		}
		else if (new_selected >= active_menu_items_)
		{
			selected_ = 0;
		}
		else
		{
			selected_ = new_selected;
		}

		// set new selected colour to selected
		menu_items_[selected_].set_colour(selected_text_colour_);
	}

	void TextMenu::SetSelectedText(int new_text)
	{
		// convert the int to a string
		string new_string = ToString(new_text);
		// assign new value to selected text
		SetSelectedText(new_string);
	}

	void TextMenu::SetSelectedText(string new_text)
	{
		// set selected text to new text
		menu_items_[selected_].set_text(new_text);
	}
}