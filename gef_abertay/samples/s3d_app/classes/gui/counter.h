#ifndef _COUNTER_H
#define _COUNTER_H

#include <iostream>
#include <string>
#include <sstream>
#include <graphics\sprite.h>
#include "gui.h"
#include "text.h"

using std::string;

/// As a child of GUI Counters are positioned based on an 'anchor' and 'offset' (see GUI base class).
/// Counters contain a text object.  The text it displays is comprised of a string and a float, being the title and count respectively

namespace s3d_app
{
	class Counter : public GUI
	{
	public:
		Counter();
		~Counter();

		// Initialise counter
		void InitCounter(gef::Vector4 anchor_pos, float x_off, float y_off, string title_text, float scale, UInt32 colour, gef::TextJustification justify);
		// Update counter with the new count
		void UpdateCounter(float count);

		// From base class
		virtual void Update(gef::Vector4 camera_pos);
		virtual void Render(gef::SpriteRenderer* sr, gef::Font* font);
		virtual void AlignToPosition(gef::Vector4 pos);

		// Get counter's text
		inline Text& text() { return text_; }				

		// get the colour of the text (ABGR)
		inline const UInt32 colour() const { return colour_; }
		// set the new colour of the text (ABGR)
		void SetColour(const UInt32 colour);
	
	private:
		// Updates the text in respect of count
		void UpdateText();									

		// Counter's text
		Text text_;			
		// Counter text's colour
		UInt32 colour_;										
		// the fixed text for the counter
		string fixed_text_;				
		// The value to be counted
		float count_;										
	};
}

#endif