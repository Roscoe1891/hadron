#include "text.h"

namespace s3d_app
{
	Text::Text()
	{
	}

	Text::~Text()
	{
	}

	void Text::InitText(gef::Vector4 anchor_pos, float x_off, float y_off, string text, float scale, UInt32 colour, gef::TextJustification justify)
	{
		Init(anchor_pos, x_off, y_off);
		text_ = text;
		scale_ = scale;
		colour_ = colour;
		justification_ = justify;
	}

	void Text::Update(gef::Vector4 camera_pos)
	{}

	// align text to provided position, relative to its offsets
	void Text::AlignToPosition(gef::Vector4 pos)
	{
		set_position(pos.x() + x_offset(), pos.y() + y_offset(), k_z_position_);
	}

	void Text::Render(gef::SpriteRenderer* sr, gef::Font* font)
	{
		// Draw text
		font->RenderText(sr, position(), scale_, colour_, justification_, "%s", text_.c_str());
	}

	void Text::set_text_from_int(int text)
	{
		text_ = ToString(text);
	}

}
