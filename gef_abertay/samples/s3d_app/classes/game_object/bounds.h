#ifndef _BOUNDS_H
#define _BOUNDS_H

#include "game_object.h"

/// Bounds are used to detect the edge of the play area
/// As objects are only capable of unrestricted movement along the z-axis,
/// only near and far bounds are required

namespace s3d_app
{
	class Bounds : public GameObject
	{
	public:
		// Bounds Type
		enum BoundsType { NEAR_BOUNDS, FAR_BOUNDS, MAX_BOUNDS };

		Bounds();
		virtual~Bounds();

		// From base class
		virtual void Update(float dt);
		virtual void OnCollision(GO_Type other);
		virtual void Die(GO_Type killed_by);

		// Get bounds type
		inline BoundsType bounds_type() const { return bounds_type_; }
		// Set bounds type
		inline void set_bounds_type(BoundsType new_type) { bounds_type_ = new_type; }

	private:
		// This Bounds' bounds type
		BoundsType bounds_type_;
	};

}

#endif