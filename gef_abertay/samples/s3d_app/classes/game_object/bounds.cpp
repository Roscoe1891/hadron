#include "bounds.h"

namespace s3d_app
{
	Bounds::Bounds()
	{
		go_type_ = BOUNDS;
		go_type_killed_by_ = NO_TYPE;
	}

	Bounds::~Bounds()
	{
	}

	void Bounds::Update(float dt)
	{
	}

	void Bounds::OnCollision(GO_Type other)
	{
	}

	void Bounds::Die(GO_Type killed_by)
	{
	}

}