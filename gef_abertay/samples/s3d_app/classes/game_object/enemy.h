#ifndef _ENEMY_H
#define _ENEMY_H

#include "actor.h"

/// Enemies are adversarial actors which move according to their behaviour type
/// The also fire their projectiles according to a timer, so long as their line of sight is not blocked

namespace s3d_app
{
	class Enemy : public Actor
	{
	public:
		// Enemy Types:
		// Standard - enemy moves in a straight line
		// Crawler - enemy will move to an adjacent level face based on a timer (and value of 'moving_clockwise')
		// Enemy Type is assigned by Enemy Manager when the enemy is spawned
		enum EnemyBehaviourType { STANDARD, CRAWLER };
	
		Enemy();
		virtual~Enemy();
		
		// From base class
		virtual void Update(float dt);
		virtual void OnCollision(GO_Type other);
		virtual void Die(GO_Type killed_by);

		// called by enemy manager to initialise this enemy during play 
		void Spawn(int starting_face, int z_pos, gef::Vector4 velocity);	
		// reset this enemy for repeated play
		void ResetEnemy();													

		// this enemy's behaviour type
		inline EnemyBehaviourType enemy_behaviour_type() { return enemy_behaviour_type_; }
		// set this enemy's behaviour type
		void set_enemy_behaviour_type(EnemyBehaviourType new_type) { enemy_behaviour_type_ = new_type; }
		// set value of 'moving clockwise' variable
		void set_moving_clockwise(bool new_value) { moving_clockwise_ = new_value; }
		// set value of 'can fire' variable
		inline void set_can_fire(bool new_value) { can_fire_ = new_value; }

	private:
		// timer for firing projectiles
		float firing_timer_;		
		// delay for firing projectiles
		float firing_delay_;			
		// true if this enemy can fire
		bool can_fire_;
		// this enemy's behaviour pattern
		EnemyBehaviourType enemy_behaviour_type_;	

		// timer for enemy movement to new level face
		float move_timer_;	
		// delay for enemy movement to new level face
		float move_delay_;
		// true if this enemy moves clockwise
		bool moving_clockwise_;						
	};
}

#endif // !_ENEMY_H
