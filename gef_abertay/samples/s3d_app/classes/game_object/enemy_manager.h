#ifndef _ENEMY_MANAGER_H
#define _ENEMY_MANAGER_H

#include <assets/obj_loader.h>
#include <classes/game_object/enemy.h>
#include <classes/level/level.h>
#include <classes/utilities/rng.h>
#include <classes/physics/physics_3d.h>

/// Enemy Manager is reponsible for generating enemies and making function calls to them as a group
/// e.g. Update, Render
/// Enemy Manager also keeps a record of enemy data which is relevant for scoring purposes:
/// How many enemies and enemy projectiles have been killed by player projectiles; and
/// how many enemies and enemy projectiles breached the near bounds

namespace s3d_app
{
#define MAX_ENEMIES 12

	class EnemyManager
	{
	public:
		// Score Data Struct
		struct EnemyScoreData
		{
			int enemies_killed_by_player;
			int enemy_projectiles_killed_by_player;
			int enemies_breached;
			int enemy_projectiles_breached;
		};

		EnemyManager();
		~EnemyManager();
		// initialise enemy manager and enemies
		void Init(MeshDataManager* mesh_data_manager, int level_faces, Level::LevelGeometryType level_geo_type, int rotation_radius, bool clear_score_data = true);
		// update enemy manager and enemies
		void Update(float dt, Physics3D* physics);
		// render enemies
		void Render(gef::Renderer3D* renderer_3d);
		// reset all enemies
		void ResetEnemies();

		// Notify Enemy manager of a collision
		// This allows enemy manager to gather the relevant score data from the collision
		void CollisionNotify(GameObject* go_1, GameObject* go_2);
		// Update the enemy score data
		void UpdateEnemyScoreData(GameObject* go);

		// Return pointer to first element in enemies array
		inline Enemy* enemies() { return enemies_; }
		// Get the enemy score data held by enemy manager
		inline EnemyScoreData enemy_score_data() const { return enemy_score_data_; }
		// Get the enemy kill count
		inline int enemy_kill_count() const { return enemy_kill_count_; }

	private:
		// Initialise enemies
		void InitEnemies(MeshDataManager* mesh_data_manager, int level_faces, Level::LevelGeometryType level_geo_type, int rotation_radius);
		// Spawn an enemy
		bool SpawnEnemy();
		// Check whether a given enemy can fire their weapon
		void CheckEnemyCanFire(Enemy* enemy, Physics3D* physics);
		// Wipe enemy score data
		void ClearEnemyScoreData();
		
		// the number of faces for the current level 
		int current_level_faces_;

		// the standard model to be used for enemies
		gef::Model enemy_model_;			
		// all enemies that could exist in the game world
		Enemy enemies_[MAX_ENEMIES];		

		// random number generator
		RNG rng_;							

		// z-axis position at which enemies will be spawned
		const int k_spawn_z_pos_;			

		// the max delay between spawning enemies
		float max_enemy_spawn_delay_;	
		// the mind delay between spawning enemies
		float min_enemy_spawn_delay_;	
		// the current delay between spawning enemies
		float current_enemy_spawn_delay_;	
		// timer for spawning enemies
		float enemy_spawn_timer_;			

		// the time interval at which the enemy spawn delay will be decreased
		float decrease_enemy_spawn_delay_interval_;
		// the timer used to track the time interval at which the enemy spawn delay will be decreased
		float decrease_enemy_spawn_delay_timer_;

		// The Score Data held by enemy manager
		EnemyScoreData enemy_score_data_;
		// The points value of an enemy
		const int k_enemy_score_points_;
		// The points value of an enemy projectile
		const int k_enemy_projectile_score_points_;

		// The number of enemies which have been killed (by any means)
		int enemy_kill_count_;
	};
}

#endif
