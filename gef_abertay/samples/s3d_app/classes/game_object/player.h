#ifndef _PLAYER_H
#define _PLAYER_H

#include "actor.h"
#include <classes/utilities/game_data.h>
#include "input/keyboard.h"

namespace s3d_app
{
	/// The user's representation in the game world.
	/// Player is an actor controlled by the user.

	class Player : public Actor
	{
	public:
		// Score Update data
		struct ScoreUpdate
		{
			int destroyed_enemies;
			int destroyed_projectiles;
			int breached_enemies;
			int breached_projectiles;
		};

		Player();
		virtual~Player();

		// From Base Class
		virtual void OnCollision(GO_Type other);
		virtual void Die(GO_Type killed_by);

		// Player-specific initialisation
		void InitPlayer(gef::Mesh* mesh, gef::Mesh* shadow, GameData* game_data, int level_faces, int starting_face, Level::LevelGeometryType level_geo_type, int rotation_radius, int z_pos, bool refresh_lives = true);
		// Update the player
		void UpdatePlayer(float dt, gef::Keyboard* keys);
		// Update Score from (param) score update
		// This function wipes and recalculates the score
		void UpdateScore(ScoreUpdate score_update);
		// Reset Player for repeat play
		void ResetPlayer();
		
		// Get player's current lives
		inline int lives() const { return lives_; }
		// Get player's current score
		inline int score() const { return score_; }

	private:
		// returns player starting lives, adjusted for difficulty
		int GetStartingLives();
		// pointer to game data
		GameData* game_data_;
		// the number of lives with which player begins the game
		const int k_starting_lives_;
		// the player's score on starting the game
		const int k_starting_score_;
		// the player's current lives
		int lives_;
		// the player's current score
		int score_;

	};

}

#endif