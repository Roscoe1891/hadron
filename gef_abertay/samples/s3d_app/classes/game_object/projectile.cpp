#include "projectile.h"

namespace s3d_app
{
	Projectile::Projectile()
	{
		go_type_ = NO_TYPE;
		go_type_killed_by_ = NO_TYPE;

		live_ = false;
	}

	Projectile::~Projectile()
	{
	}

	void Projectile::Fire(gef::Vector4 position)
	{
		// Set position (should be the firing actor's position)
		SetPosition(position);
		// set to live
		live_ = true;
		go_type_killed_by_ = NO_TYPE;
	}

	void Projectile::Update(float dt)
	{
		// move live projectile 
		if (live_)
		{
			Move(dt);
		}
	}

	void Projectile::OnCollision(GO_Type other)
	{
		// kill projectile
		Die(other);
	}

	void Projectile::Die(GO_Type killed_by)
	{
		live_ = false;
		go_type_killed_by_ = killed_by;
	}

}