#include "player.h"

namespace s3d_app
{
	Player::Player() : k_starting_lives_(3), k_starting_score_(100)
	{
		go_type_ = PLAYER;
		go_type_killed_by_ = NO_TYPE;

		firing_velocity_ = gef::Vector4(0.f, 0.f, -100.f);
		lives_ = k_starting_lives_;
		score_ = k_starting_score_;
	}

	Player::~Player()
	{		
	}

	void Player::InitPlayer(gef::Mesh* mesh, gef::Mesh* shadow, GameData* game_data, int level_faces, int starting_face, Level::LevelGeometryType level_geo_type, int rotation_radius, int z_pos, bool refresh_lives)
	{
		// set game data
		game_data_ = game_data;
		// set starting lives
		if (refresh_lives) lives_ = GetStartingLives();
		// Call Base Init
		InitActor(mesh, shadow, level_faces, starting_face, level_geo_type, rotation_radius, z_pos);
		// player starts live
		live_ = true;
	}

	void Player::UpdatePlayer(float dt, gef::Keyboard* keys)
	{
		// check keyboard input
		if (keys->IsKeyPressed(keys->KC_D))
		{
			// move anti-clockwise
			MoveToNewFace(false);
		}
		if (keys->IsKeyPressed(keys->KC_A))
		{
			// move clockwise
			MoveToNewFace(true);
		}
		if (keys->IsKeyPressed(keys->KC_S))
		{
			// fire a projectile
			FireProjectile();
		}

		// actor update
		Update(dt);
	}

	void Player::OnCollision(GO_Type other)
	{
		// Deduct lives if possible, otherwise die
		if (lives_ > 0)
			lives_--;
		else
			Die(other);
	}

	void Player::Die(GO_Type killed_by)
	{
		live_ = false;
		go_type_killed_by_ = killed_by;
	}

	void Player::ResetPlayer()
	{
		// Reset progress
		lives_ = GetStartingLives();
		score_ = k_starting_score_;

		// Reset position / location
		current_level_face_ = starting_level_face_;
		gef::Vector2 new_pos_xy = CalcPosition();
		SetPosition(gef::Vector4(new_pos_xy.x, new_pos_xy.y, position_.z()));
		SetRotation_Z(0.f);

		// reset projectiles
		ResetProjectiles();

		// default is live
		live_ = true;
		go_type_killed_by_ = NO_TYPE;
	}

	void Player::UpdateScore(ScoreUpdate score_update)
	{
		// reset to starting value
		score_ = k_starting_score_;
		// add points for destroyed enemies and projectiles
		score_ += (score_update.destroyed_enemies);
		score_ += (score_update.destroyed_projectiles);
		// deduct points for enemies and projectiles which got past the player
		score_ -= (score_update.breached_enemies);
		score_ -= (score_update.breached_projectiles);

		// Player dies if score < 0!
		if (score_ < 0)
		{
			// don't show a negative score
			score_ = 0;
			Die(BOUNDS);
		}
	}

	int Player::GetStartingLives()
	{
		// calculate starting lives based on game difficulty
		int result;
		switch (game_data_->difficulty())
		{
		case GameData::EASY:
			result = k_starting_lives_ + 2;
			break;
		case GameData::NORMAL:
			result = k_starting_lives_;
			break;
		case GameData::HARD:
			result = k_starting_lives_ - 2;
			break;
		default:
			break;
		}
		return result;
	}



}