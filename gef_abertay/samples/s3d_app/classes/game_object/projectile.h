#ifndef _PROJECTILE_H
#define _PROJECTILE_H

#include "game_object.h"

/// Projectiles are missiles fired by the player and enemies
/// When live, they move at a constant velocity and are destroyed upon collision

namespace s3d_app
{
	class Projectile : public GameObject
	{
	public:
		Projectile();
		virtual~Projectile();

		// From base class
		virtual void Update(float dt);
		virtual void OnCollision(GO_Type other);
		virtual void Die(GO_Type killed_by);

		// Fire this projectile
		void Fire(gef::Vector4 position);

		// Set this projectile's game object type
		inline void set_GO_type(GO_Type new_GO_type) { go_type_ = new_GO_type; }

	private:

	};
}

#endif // !_PROJECTILE_H
