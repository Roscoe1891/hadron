#include "actor.h"

namespace s3d_app
{
	Actor::Actor() : k_shadow_offset_(gef::Vector4(0.f, -4.5f, 0.f))
	{
		current_level_face_ = 0;
	}

	Actor::~Actor()
	{
	}

	void Actor::InitActor(gef::Model* model, gef::Mesh* shadow_mesh, int level_faces, int starting_face, Level::LevelGeometryType level_geo_type, int rotation_radius, int z_pos)
	{
		InitActor(model->mesh(), shadow_mesh, level_faces, starting_face, level_geo_type, rotation_radius, z_pos);
	}

	void Actor::InitActor(gef::Mesh* mesh, gef::Mesh* shadow_mesh, int level_faces, int starting_face, Level::LevelGeometryType level_geo_type, int rotation_radius, int z_pos)
	{
		// Assign positional info
		current_level_face_ = starting_face;
		current_level_faces_ = level_faces;
		starting_level_face_ = starting_face;
		current_level_geometry_type_ = level_geo_type;
		rotation_radius_ = rotation_radius;
		
		// Not live by default
		live_ = false;

		// set starting rotation
		float rotation_z = CalcRotation_Z();
		SetRotation_Z(rotation_z);

		// set starting positon
		gef::Vector2 start_pos_xy = CalcPosition();
		gef::Vector4 start_pos = gef::Vector4(start_pos_xy.x, start_pos_xy.y, z_pos);

		// call base init
		Init(mesh, start_pos, gef::Vector4(0.f, 0.f, 0.f));

		// Set scale
		SetScale(0.58);

		// init projectiles
		InitProjectiles(mesh);

		// init shadow
		shadow_.Init(shadow_mesh, k_shadow_offset_, transform_);
	}


	gef::Vector2 Actor::CalcPosition()
	{
		gef::Vector2 result;
		double angle;
		float angle_offset;

		// check level geometry type
		switch (current_level_geometry_type_)
		{
		case Level::CIRCULAR:
			// Calc sector angle
			angle_offset = (2 * (float)PI) / current_level_faces_;
			// Calc actor's current angle around circle circumference
			angle = angle_offset * current_level_face_;
			// Calculate position based on angle and rotation radius
			result = gef::Vector2(((float)cos(angle) * rotation_radius_), ((float)sin(angle) * rotation_radius_));
			break;
		default:
			result = gef::Vector2(0.f, 0.f);
			break;
		}

		return result;
	}

	float Actor::GetRotation_Z_Increment()
	{
		float result = 0.f;

		// check geometry type
		switch (current_level_geometry_type_)
		{
		case Level::CIRCULAR:
			// calc sector angle
			result = (2 * (float)PI) / current_level_faces_;
			break;
		default:
			break;
		}

		return result;
	}

	float Actor::CalcRotation_Z()
	{
		float result = 0.f;

		// check level geometry type
		switch (current_level_geometry_type_)
		{
		case Level::CIRCULAR:
			// calc sector angle
			result = (2 * (float)PI) / current_level_faces_;	
			// multiply by current occupied sector
			result *= current_level_face_;	
			// adjust to orient around 0 rotation being at "6 o'clock" (not "3 o'clock")
			result += ((float)PI / 2);							
			break;
		default:
			break;
		}
		
		return result;

	}

	void Actor::InitProjectiles(gef::Model* model)
	{
		InitProjectiles(model->mesh());
	}

	void Actor::InitProjectiles(gef::Mesh* mesh)
	{
		for (int i = 0; i < MAX_AMMO; i++)
		{
			// projectile initialisation
			projectiles_[i].Init(mesh, position_, firing_velocity_);

			// type-specific assignments
			switch (go_type_)
			{
			case PLAYER:
				projectiles_[i].set_GO_type(PLAYER_PROJECTILE);
				break;
			case ENEMY:
				projectiles_[i].set_GO_type(ENEMY_PROJECTILE);
				break;
			}
			// set projectile scale
			projectiles_[i].SetScale(0.2f);
			// projectiles are not live1 by default
			projectiles_[i].set_live(false);
		}
	}

	void Actor::ResetProjectiles()
	{
		// kill all projectiles
		for (int i = 0; i < MAX_AMMO; i++)
		{
			projectiles_[i].set_live(false);
		}
	}

	void Actor::MoveToNewFace(bool clockwise)
	{
		// check direction of movement
		// increment or decrement current level face
		if (clockwise) current_level_face_--;
		else current_level_face_++;
		
		// get and set new z rotation
		float rotation_z = CalcRotation_Z();
		SetRotation_Z(rotation_z);
		// get and set new position
		gef::Vector2 new_pos_xy = CalcPosition();
		SetPosition(gef::Vector4(new_pos_xy.x, new_pos_xy.y, position_.z()));
	}

	void Actor::FireProjectile()
	{
		// search projectiles
		for (int i = 0; i < MAX_AMMO; i++)
		{
			// if not live
			if (!projectiles_[i].live())
			{
				// fire
				projectiles_[i].Fire(position_);
				break;
			}
		}
	}

	void Actor::Render(gef::Renderer3D* renderer_3d)
	{
		// Draw this actor's mesh
		renderer_3d->DrawMesh(*this);

		// draw projectiles
		RenderProjectiles(renderer_3d);

		// draw shadow
		shadow_.Render(renderer_3d);
	}

	void Actor::RenderProjectiles(gef::Renderer3D* renderer_3d)
	{
		for (int i = 0; i < MAX_AMMO; i++)
		{
			// only draw live projectiles
			if (projectiles_[i].live())
				projectiles_[i].Render(renderer_3d);
		}
	}

	void Actor::Update(float dt)
	{
		// update projectiles
		UpdateProjectiles(dt);
		// update shadow
		shadow_.Update(transform_);
	}

	void Actor::UpdateProjectiles(float dt)
	{
		for (int i = 0; i < MAX_AMMO; i++)
		{
			// only update live projectiles
			if (projectiles_[i].live())
				projectiles_[i].Update(dt);
		}
	}

	void Actor::Die(GO_Type killed_by)
	{
		// inherited classes may overrride this function
	}

	void Actor::OnCollision(GO_Type other)
	{
		// inherited classes may overrride this function
	}

}