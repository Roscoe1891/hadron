#ifndef _GAME_OBJECT_H
#define _GAME_OBJECT_H

#include <maths/quaternion.h>
#include <graphics/mesh_instance.h>
#include <graphics/model.h>
#include <graphics/renderer_3d.h>
#include <classes/utilities/math_utilities.h>

/// Game Object is the base class for players, enemies and projectiles - i.e. any object within the game world with which the player can interact

namespace s3d_app
{
	class GameObject : public gef::MeshInstance
	{
	public:
		// Game Object Type
		enum GO_Type { NO_TYPE, PLAYER, ENEMY, PLAYER_PROJECTILE, ENEMY_PROJECTILE, BOUNDS };

		GameObject();
		virtual~GameObject();

		// Update this game object
		virtual void Update(float dt) = 0;
		// Called upon collision with object of type 'other'
		virtual void OnCollision(GO_Type other) = 0;
		// Called upon being destroyed by object of type 'killed by'
		virtual void Die(GO_Type killed_by) = 0;
		// Draw this game object
		virtual void Render(gef::Renderer3D* renderer_3d);
		
		// Initialise this game object from a mesh
		void Init(gef::Mesh* mesh, gef::Vector4 position, gef::Vector4 velocity);
		// Initialise this game object from a model
		void Init(gef::Model* model, gef::Vector4 position, gef::Vector4 velocity);
		
		// Set game object's velocity
		inline void SetVelocity(gef::Vector4 velocity) { velocity_ = velocity; }
		// Get game object's velocity
		inline gef::Vector4 velocity() const { return velocity_; }
		// Set game object's position
		void SetPosition(gef::Vector4 new_position);
		// Get game object's position
		inline gef::Vector4 position() const { return position_; }
		// Set game object's rotation around the z axis
		void SetRotation_Z(float new_rotation);
		// Get game object's rotation around the z axis
		inline float rotation_z() const { return rotation_z_; }
		// Set game object scale
		void SetScale(float new_scale);
		// Get game object scale
		inline float scale() { return scale_; }

		// Get this game object's type
		inline GO_Type go_type() const { return go_type_; }
		// Set this game object's type
		inline GO_Type go_type_killed_by() const { return go_type_killed_by_; }

		// get value of live
		inline bool live() const { return live_; }
		// set value of live
		inline void set_live(bool new_live) { live_ = new_live; }

	protected:
		// Update this game object's transform matrix
		// (This function rebuilds the transform matrix in doing so)
		void UpdateTransform();
		// Move this game object
		void Move(float dt);

		// this game object's type
		GO_Type go_type_;		
		// type of game object this game object was killed by
		GO_Type go_type_killed_by_;		

		// game object's velocity
		gef::Vector4 velocity_;		
		// game object's position in world space
		gef::Vector4 position_;		
		// game object's rotation about the z axis
		float rotation_z_;	
		// game object's uniform scaling factor
		float scale_;					

		// true if game object is considered to be alive for gameplay purposes
		bool live_;						
	};

}

#endif
