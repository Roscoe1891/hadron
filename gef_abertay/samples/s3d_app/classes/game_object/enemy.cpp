#include "enemy.h"

namespace s3d_app
{
	Enemy::Enemy()
	{
		// set initial / default values
		go_type_ = ENEMY;
		go_type_killed_by_ = NO_TYPE;
		enemy_behaviour_type_ = STANDARD;

		firing_timer_ = 0.f;
		firing_delay_ = 2.f;
		firing_velocity_ = gef::Vector4(0.f, 0.f, 100.f);
		can_fire_ = false;

		move_timer_ = 0.f;
		move_delay_ = 3.f;
		moving_clockwise_ = false;
	}

	Enemy::~Enemy()
	{
	}

	void Enemy::Update(float dt)
	{
		// Check for firing projectiles
		if (firing_timer_ < firing_delay_)
		{
			firing_timer_ += dt;
		}
		else
		{
			if (can_fire_) FireProjectile();
			
			firing_timer_ = 0.f;
		}

		// Call base update
		Actor::Update(dt);

		// Check for enemy specific updates
		switch (enemy_behaviour_type_)
		{
		case s3d_app::Enemy::STANDARD:
			break;
		case s3d_app::Enemy::CRAWLER:
			// Check for movement
			if (move_timer_ < move_delay_)
			{
				move_timer_ += dt;
			}
			else
			{
				MoveToNewFace(moving_clockwise_);
				move_timer_ = 0.f;
			}
			break;
		default:
			break;
		}

		// Move enemy
		Move(dt);
	}

	void Enemy::OnCollision(GO_Type other)
	{
		// Kill enemy
		Die(other);
	}

	void Enemy::Die(GO_Type killed_by)
	{
		live_ = false;
		go_type_killed_by_ = killed_by;
		ResetProjectiles();
	}

	void Enemy::Spawn(int starting_face, int z_pos, gef::Vector4 velocity)
	{	
		// set position
		current_level_face_ = starting_face;
		gef::Vector2 start_pos_xy = CalcPosition();
		gef::Vector4 start_pos = gef::Vector4(start_pos_xy.x, start_pos_xy.y, z_pos);
		SetPosition(start_pos);

		// set rotation
		float rotation_z = CalcRotation_Z();
		SetRotation_Z(rotation_z);

		// set velocity
		velocity_ = velocity;

		// set to live
		live_ = true;
		go_type_killed_by_ = NO_TYPE;
	}

	void Enemy::ResetEnemy()
	{
		// enemy is not live
		live_ = false;
		// reset enemy projectiles
		ResetProjectiles();
	}

}