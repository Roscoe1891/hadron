#include "game_object.h"

namespace s3d_app
{
	GameObject::GameObject()
	{
		live_ = false;
		go_type_killed_by_ = NO_TYPE;
		rotation_z_ = 0;
		scale_ = 1;
	}

	GameObject::~GameObject()
	{
	}

	void GameObject::Init(gef::Model* model, gef::Vector4 position, gef::Vector4 velocity)
	{
		Init(model->mesh(), position, velocity);
	}

	void GameObject::Init(gef::Mesh* mesh, gef::Vector4 position, gef::Vector4 velocity)
	{
		// assign mesh from model
		set_mesh(mesh);

		// set position
		position_ = position;
		
		// set transform matrix
		UpdateTransform();

		// set velocity
		velocity_ = velocity;
	}

	void GameObject::Move(float dt)
	{
		// Calc new position
		position_ += (velocity_ * dt);
		// Set translation of transform matrix
		transform_.SetTranslation(position_);
	}

	void GameObject::UpdateTransform()
	{
		// Wipe transform
		transform_.SetIdentity();

		// Update translation
		transform_.SetTranslation(position_);

		// Update Rotation
		gef::Matrix44 rotation_matrix;
		rotation_matrix.RotationZ(rotation_z_);
		transform_ = rotation_matrix * transform_;

		// Update Scale
		gef::Matrix44 scale_matrix;
		scale_matrix.Scale(gef::Vector4(scale_, scale_, scale_));
		transform_ = scale_matrix * transform_;
	}

	void GameObject::SetPosition(gef::Vector4 new_position)
	{
		// update position
		position_ = new_position;
		// set translation of transform matrix
		transform_.SetTranslation(position_);
	}

	void GameObject::SetRotation_Z(float new_rotation)
	{
		// update rotation
		rotation_z_ = new_rotation;
		// update transform matrix
		UpdateTransform();
	}

	void GameObject::SetScale(float new_scale)
	{
		// update scale
		scale_ = new_scale;
		// update transform matrix
		UpdateTransform();
	}

	void GameObject::Render(gef::Renderer3D* renderer_3d)
	{
		// draw game object mesh
		renderer_3d->DrawMesh(*this);
	}


}