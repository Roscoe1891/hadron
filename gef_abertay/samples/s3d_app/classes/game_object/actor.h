#ifndef _ACTOR_H
#define _ACTOR_H

#include "game_object.h"
#include "projectile.h"
#include <classes/level/level.h>
#include <classes/graphics/shadow_imposter.h>
#include <maths/vector2.h>
#include <classes/physics/physics_3d.h>

/// An actor is an active participant in the game world capable of firing projectiles 
/// such as the player or enemies, derived from GameObject

namespace s3d_app
{

#define MAX_AMMO 6

	class Actor : public GameObject
	{
	public:
		Actor();
		virtual~Actor();
		
		// From base class
		virtual void Update(float dt);
		virtual void OnCollision(GO_Type other);
		virtual void Die(GO_Type killed_by);

		// Initialise actor using model
		virtual void InitActor(gef::Model* model, gef::Mesh* shadow_mesh, int level_faces, int starting_face, Level::LevelGeometryType level_geo_type, int rotation_radius, int z_pos);
		// Initialise actor using mesh
		virtual void InitActor(gef::Mesh* mesh, gef::Mesh* shadow_mesh, int level_faces, int starting_face, Level::LevelGeometryType level_geo_type, int rotation_radius, int z_pos);

		// calculate actor's position based on current level geometry type
		gef::Vector2 CalcPosition();
		// calculate actor's rotation about the z axis based on current level geometry type
		float CalcRotation_Z();
		// get the angle increment to be used in rotation calculations
		float GetRotation_Z_Increment();
		// Move this actor one level face clockwise or anticlockwise
		void MoveToNewFace(bool clockwise);
		// Render this actotr
		void Render(gef::Renderer3D* renderer_3d);
		// Fire a projectile
		void FireProjectile();
		// Return pointer to start of projectile array
		inline Projectile* projectiles() { return projectiles_; }

	protected:
		// Initialise actor's projectiles from model
		void InitProjectiles(gef::Model* model);
		// Initialise actor's projectiles from mesh
		void InitProjectiles(gef::Mesh* mesh);						
		// Reset actor's projectiles
		void ResetProjectiles();		
		// Update actor's projectiles
		void UpdateProjectiles(float dt);	
		// Draw actor's projectiles
		void RenderProjectiles(gef::Renderer3D* renderer_3d);		
		// which face of the current level's geometry the actor is occupying
		int current_level_face_;	
		// number of faces of the current level's geometry
		int current_level_faces_;	
		// the face of the current level's geometry on which the actor was initialised
		int starting_level_face_;	
		// the type of geometry of the current level
		Level::LevelGeometryType current_level_geometry_type_;	
		// size of radius around which player rotates round the level centre
		int rotation_radius_;			
		// The actor's projectiles
		Projectile projectiles_[MAX_AMMO];
		// The velocity at which this actor fires projectiles
		gef::Vector4 firing_velocity_;
		// The actor's shadow
		ShadowImposter shadow_;
		// Position offset for the actor's shadow
		gef::Vector4 k_shadow_offset_;
	};

}

#endif // !_PLAYER_H
