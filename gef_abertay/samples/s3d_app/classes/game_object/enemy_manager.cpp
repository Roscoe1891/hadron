#include "enemy_manager.h"

namespace s3d_app
{
	EnemyManager::EnemyManager() : k_spawn_z_pos_(-50), k_enemy_score_points_(50), k_enemy_projectile_score_points_(25)
	{
		// set initial and default values

		current_level_faces_ = 0;

		min_enemy_spawn_delay_ = 0.5f;
		max_enemy_spawn_delay_ = 3.f;
		current_enemy_spawn_delay_ = max_enemy_spawn_delay_;
		enemy_spawn_timer_ = 0.f;

		decrease_enemy_spawn_delay_interval_ = 5.f;
		decrease_enemy_spawn_delay_timer_ = 0.f;

		enemy_kill_count_ = 0;
		ClearEnemyScoreData();
	}

	EnemyManager::~EnemyManager()
	{
	}

	void EnemyManager::Init(MeshDataManager* mesh_data_manager, int level_faces, Level::LevelGeometryType level_geo_type, int rotation_radius, bool clear_score_data)
	{
		// Initialise enemies
		InitEnemies(mesh_data_manager, level_faces, level_geo_type, rotation_radius);
		
		// Clear Score Data if required
		if (clear_score_data) ClearEnemyScoreData();

		// assign init values
		current_level_faces_ = level_faces;
		enemy_spawn_timer_ = 0.f;
		enemy_kill_count_ = 0;
		current_enemy_spawn_delay_ = max_enemy_spawn_delay_;
		decrease_enemy_spawn_delay_timer_ = 0.f;
	}

	void EnemyManager::ResetEnemies()
	{
		// call reset on all enemies
		for (int i = 0; i < MAX_ENEMIES; ++i)
		{
			enemies_[i].ResetEnemy();
		}

		// reset enemy spawn delay
		current_enemy_spawn_delay_ = max_enemy_spawn_delay_;
		enemy_spawn_timer_ = 0.f;

		decrease_enemy_spawn_delay_timer_ = 0.f;
		
		// reset enemy kill count
		enemy_kill_count_ = 0;

		// clear score data
		ClearEnemyScoreData();
	}

	void EnemyManager::ClearEnemyScoreData()
	{
		// set all values to 0
		enemy_score_data_.enemies_breached = 0;
		enemy_score_data_.enemies_killed_by_player = 0;
		enemy_score_data_.enemy_projectiles_breached = 0;
		enemy_score_data_.enemy_projectiles_killed_by_player = 0;
	}

	void EnemyManager::Update(float dt, Physics3D* physics)
	{
		// check for decrease enemy spawn delay
		if (decrease_enemy_spawn_delay_timer_ < decrease_enemy_spawn_delay_interval_)
			decrease_enemy_spawn_delay_timer_ += dt;
		else
		{
			// prevent decrement from continuing indefinitely
			if (current_enemy_spawn_delay_ > min_enemy_spawn_delay_)
			{
				current_enemy_spawn_delay_ -= 0.5f;
				decrease_enemy_spawn_delay_timer_ = 0;
			}
		}

		// check for enemy spawning
		if (enemy_spawn_timer_ < current_enemy_spawn_delay_)
			enemy_spawn_timer_ += dt;
		else
			SpawnEnemy();

		// update live enemies
		for (int i = 0; i < MAX_ENEMIES; ++i)
		{
			if (enemies_[i].live())
			{
				// check if enemy can fire
				CheckEnemyCanFire(&enemies_[i], physics);
				// update enemy
				enemies_[i].Update(dt);
			}
				
		}
	}

	// Check whether an enemy can fire and set their 'can_fire' tag accordingly
	void EnemyManager::CheckEnemyCanFire(Enemy* enemy, Physics3D* physics)
	{
		// Create ray to be passed to physics
		Ray ray;
		ray.origin = enemy->position();
		ray.direction = enemy->velocity();		
		ray.direction.Normalise();

		// assume enemy can fire
		enemy->set_can_fire(true);

		// check through all live enemies
		for (int i = 0; i < MAX_ENEMIES; ++i)
		{
			if (enemies_[i].live())
			{
				// (that aren't this enemy)
				if (&enemies_[i] != enemy)
				{
					// if ray from this enemy intesects with any enemy's bounding sphere, this enemy can't fire
					if (physics->RayCastToSphere(ray, &enemies_[i]))
					{
						enemy->set_can_fire(false);
						break;
					}
				}	
			}
		}
	}

	// The majority of collisions involve an enemy or projectile.
	// This notification allows enemy manager to do any necessary admin or tidying up immediately following a collision,
	// specifically updating enemy score data
	void EnemyManager::CollisionNotify(GameObject* go_1, GameObject* go_2)
	{
		if ((go_1->go_type() == GameObject::ENEMY) || (go_1->go_type() == GameObject::ENEMY_PROJECTILE)) UpdateEnemyScoreData(go_1);
		if ((go_2->go_type() == GameObject::ENEMY) || (go_2->go_type() == GameObject::ENEMY_PROJECTILE)) UpdateEnemyScoreData(go_2);
	}

	// Update Enemy Score Data in relation to (param) Game Object
	void EnemyManager::UpdateEnemyScoreData(GameObject* go)
	{
		// check this game object's type
		switch (go->go_type())
		{
		case GameObject::ENEMY:
			// an enemy will be destroyed after any collision, so increment kill count
			enemy_kill_count_++;

			// check what killed this enemy and adjust score data accordingly
			switch (go->go_type_killed_by())
			{
			case GameObject::PLAYER_PROJECTILE:
				enemy_score_data_.enemies_killed_by_player += k_enemy_score_points_;
				break;
			case GameObject::BOUNDS:
				enemy_score_data_.enemies_breached += k_enemy_score_points_;
				break;
			default:
				break;
			}

			break;

		case GameObject::ENEMY_PROJECTILE:
			// check what killed this enemy projectile and adjust score data accordingly
			switch (go->go_type_killed_by())
			{
			case GameObject::PLAYER_PROJECTILE:
				enemy_score_data_.enemy_projectiles_killed_by_player += k_enemy_projectile_score_points_;
				break;
			case GameObject::BOUNDS:
				enemy_score_data_.enemy_projectiles_breached += k_enemy_projectile_score_points_;
				break;
			default:
				break;
			}

			break;

		default:
			break;
		}
	}

	void EnemyManager::Render(gef::Renderer3D* renderer_3d)
	{
		// render live enemies
		for (int i = 0; i < MAX_ENEMIES; ++i)
		{
			if (enemies_[i].live())
				enemies_[i].Render(renderer_3d);
		}
	}

	
	void EnemyManager::InitEnemies(MeshDataManager* mesh_data_manager, int level_faces, Level::LevelGeometryType level_geo_type, int rotation_radius)
	{
		// Initialise all enemies
		for (int i = 0; i < MAX_ENEMIES; ++i)
		{
			enemies_[i].InitActor(mesh_data_manager->scene_meshes()[MeshDataManager::ENEMY_MESH],
				mesh_data_manager->scene_meshes()[MeshDataManager::SHADOW_IMPOSTER_MESH],
				level_faces, 0, level_geo_type, rotation_radius, k_spawn_z_pos_);
		}
	}

	bool EnemyManager::SpawnEnemy()
	{
		bool result = false;

		for (int i = 0; i < MAX_ENEMIES; ++i)
		{
			// find an enemy which is not live
			if (!enemies_[i].live())
			{
				// get a random number corresponding to level face
				int face = rng_.Random() % current_level_faces_;
				enemies_[i].Spawn(face, k_spawn_z_pos_, gef::Vector4(0.f, 0.f, 10.f));
				
				// Set Enemy's Behaviour Type:
				// 40% chance this enemy is a crawler
				int random = rng_.Random();
				if (random % 5 < 2)
				{
					// set behavior type
					enemies_[i].set_enemy_behaviour_type(Enemy::CRAWLER);
					// randomise movement direction
					if (rng_.Random() % 2 == 1) enemies_[i].set_moving_clockwise(true);
					else enemies_[i].set_moving_clockwise(false);
				}
				// other behaviour is standard
				else enemies_[i].set_enemy_behaviour_type(Enemy::STANDARD);
				
				result = true;
				
				break;
			}
		}
		// reset timer 
		enemy_spawn_timer_ = 0.f;
		return result;
	}

}