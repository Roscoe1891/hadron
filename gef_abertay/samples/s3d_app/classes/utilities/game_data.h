#ifndef _GAME_DATA_H
#define _GAME_DATA_H

#include <string>

// Game Data should be used to store persistent (not state-unique) game data

namespace s3d_app
{
	class GameData
	{
	public:
		// enumerate game difficulty grades
		enum Difficulty { EASY, NORMAL, HARD, MAX_DIFFICULTIES };

		GameData();
		~GameData();

		// set difficulty
		inline void set_difficulty(Difficulty new_difficulty) { difficulty_ = new_difficulty; }
		// get difficulty
		inline Difficulty difficulty() const { return difficulty_; }

		// set final score
		inline void set_final_score(int new_score) { final_score_ = new_score; }
		// get final score
		inline int final_score() const { return final_score_; }

	private:
		// game difficulty
		Difficulty difficulty_;
		// player's final score on completing final level
		int final_score_;
	};
}
#endif