#ifndef _RNG_H
#define _RNG_H

#include <ctime>
#include <cstdlib>

// Random number generator

namespace s3d_app
{
	class RNG
	{
	public:
		RNG();
		~RNG();

		// return a random number
		int Random();				
		// return a random number within provided range (inclusive)
		int RandomRange(int min, int max);		

	private:

	};
}
#endif // !_RNG_H
