#include "state_start.h"

namespace s3d_app
{
	StateStart::StateStart()
	{
		state_type_ = START;
		next_state_type_ = state_type_;

		loaded_ = false;

		state_text_ = "START STATE.";
	}

	StateStart::~StateStart()
	{
	}

	void StateStart::StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		StateInit(platform, camera, game_data, mesh_data_manager);
	}

	void StateStart::StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		next_state_type_ = state_type_;
		game_data_ = game_data;

		// Init UI
		header_text_.InitText(camera->position(), platform.width()*0.5f, 50.f, "H A D R O N", 1.f, 0xffffffff, gef::TJ_CENTRE);
		progress_text_.InitText(camera->position(), platform.width()*0.5f, 490.f, "LOADING...", 1.f, 0xffffffff, gef::TJ_CENTRE);

		// If assets have not been loaded, load assets
		if (!loaded_) LoadAssets(platform, mesh_data_manager);

	}

	bool StateStart::StateUpdate(gef::InputManager* input, float dt)
	{
		// check input
		if (input)
		{
			gef::Keyboard* keys = input->keyboard();
			
			// if assets have been loaded, allow progression to next state
			if (loaded_)
			{
				if ((keys->IsKeyPressed(keys->KC_RETURN)) || (keys->IsKeyPressed(keys->KC_NUMPADENTER)))
				{
					next_state_type_ = MENU;
				}
			}
			
		}
		return true;
	}
	
	void StateStart::StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// render state UI

		// DEBUG: font->RenderText(sprite_renderer, gef::Vector4(10.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, state_text_.c_str());
		
		header_text_.Render(sprite_renderer, font);
		progress_text_.Render(sprite_renderer, font);
	}

	void StateStart::StateRender3D(gef::Renderer3D* renderer_3d)
	{
	}

	void StateStart::StateCleanUp()
	{}

	void StateStart::LoadAssets(gef::Platform& platform, MeshDataManager* mesh_data_manager)
	{
		// Load Assets here
		if (mesh_data_manager->BuildMeshes(platform));
		{
			loaded_ = true;
			progress_text_.set_text("PRESS ENTER");
		}
	}
}