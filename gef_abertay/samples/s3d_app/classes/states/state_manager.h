#ifndef _STATE_MANAGER_H
#define _STATE_MANAGER_H

#include <classes/graphics/mesh_data_manager.h>
#include <classes/graphics/s3d_renderer.h>
#include <graphics/sprite_renderer.h>
#include <graphics/font.h>
#include <input/input_manager.h>

#include "state_start.h"
#include "state_menu.h"
#include "state_play.h"
#include "state_game_over.h"
#include "state_quit.h"

// State manager encapsulates the game states and handles transitions between states

namespace s3d_app
{
	class StateManager
	{
	public:
		StateManager();
		~StateManager();

		// Initialise
		void Init(gef::Platform* platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		// Update (inc. call update on current state)
		bool Update(gef::InputManager* input, float dt);
		// Render UI (inc. call render UI on current state)
		void RenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font);

		// Get pointer to the current state
		State* GetCurrentState() const;

	private:
		// Assign states to states array
		void AssignStates();
		// Change to the next state
		void ChangeState();

		// The Game States
		StateStart start_state_;
		StateMenu menu_state_;
		StatePlay play_state_;
		StateGameOver game_over_state_;
		StateQuit quit_state_;

		// The current state
		State::StateType current_state_;
		// Array of pointers to the states
		State* states_[State::NO_STATES];

		// pointer to the s3d renderer
		S3DRenderer* s3d_renderer_;
		// pointer to game data
		GameData* game_data_;
		// pointer to the mesh data manager
		MeshDataManager* mesh_data_manager_;
		// pointer to the platform
		gef::Platform* platform_;
		// pointer to the camera
		Camera* camera_;

	};
}

#endif

