#ifndef _STATE_MENU_HELP_H
#define _STATE_MENU_HELP_H

#include "sub_state.h"


namespace s3d_app
{
	class StateMenuHelp : public SubState
	{

// class-specific definitions
#define MAX_CONTROL_TEXT 4
#define MAX_OBJECTIVE_TEXT 3

	public:
		StateMenuHelp();
		~StateMenuHelp();

		// From base class
		virtual void SubStateInit(gef::Platform& platform, Camera* camera);
		virtual void SubStateUpdate(gef::InputManager* input, float dt);
		virtual void SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void SubStateCleanUp();

	private:
		// UI elements
		Text return_to_menu_text_;
		Text controls_text_[MAX_CONTROL_TEXT];
		Text objective_text_[MAX_OBJECTIVE_TEXT];

	};
}

#endif