#ifndef _STATE_PLAY_DEAD_H
#define _STATE_PLAY_DEAD_H

#include "sub_state.h"
#include "classes/gui/text_menu.h"

// Substate governing behaviour when player has died during play

namespace s3d_app
{
	class StatePlayDead : public SubState
	{
	public:
		// enumerate menu options
		enum DeathMenuOptions { REPLAY, QUIT };

		StatePlayDead();
		~StatePlayDead();

		// From base class
		virtual void SubStateInit(gef::Platform& platform, Camera* camera);
		virtual void SubStateUpdate(gef::InputManager* input, float dt);
		virtual void SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void SubStateCleanUp();

	private:
		// the death screen menu
		TextMenu menu_;

	};
}

#endif
