#ifndef _STATE_PLAY_ACTIVE_H
#define _STATE_PLAY_ACTIVE_H

#include "sub_state.h"
#include <classes/game_object/player.h>
#include <classes/game_object/enemy_manager.h>
#include <classes/game_object/bounds.h>
#include <classes/physics/physics_3d.h>
#include <classes/gui/counter.h>
#include <classes/utilities/game_data.h>

// sub state governing behaviour during active play

namespace s3d_app
{
	class StatePlayActive : public SubState
	{
	public:
		StatePlayActive();
		~StatePlayActive();

		// From base class
		virtual void SubStateInit(gef::Platform& platform, Camera* camera);
		virtual void SubStateUpdate(gef::InputManager* input, float dt);
		virtual void SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void SubStateCleanUp();

		// substate-specific initialisation
		void ActivePlayStateInit(gef::Platform& platform, Camera* camera, GameData* game_data);
		// substate-specific update
		void ActivePlayStateUpdate(gef::InputManager* input, float dt, Player* player, EnemyManager* enemy_manager, Bounds* bounds);
		// substate-specific reset
		void ActivePlayStateReset(Player* player, EnemyManager* enemy_manager);
		// resume play from pause game
		void ResumeFromPause();

	private:
		// Call collision checks using physics 3D
		void CollisionChecks(Player* player, EnemyManager* enemy_manager, Bounds* bounds);
		// Call collision responses
		void CollisionResponses(Player* player, EnemyManager* enemy_manager);

		// Update GUI
		void UpdateGUI(Player* player);

		// send a player score update
		void SendPlayerScoreUpdate(Player* player, EnemyManager* enemy_manager);
		// create a player score update from an enemy score data 
		Player::ScoreUpdate CreatePlayerScoreUpdate(EnemyManager::EnemyScoreData enemy_score_data);

		// pointer to game data
		GameData* game_data_;

		// Physics 3D
		Physics3D physics_;

		// UI representing player score
		Counter score_counter_;
		// UI representing player lives
		Counter lives_counter_;

	};
}

#endif