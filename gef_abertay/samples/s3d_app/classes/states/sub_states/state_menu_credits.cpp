#include "state_menu_credits.h"

namespace s3d_app
{
	StateMenuCredits::StateMenuCredits()
	{
		sub_state_type_ = MENU_CREDITS;
		next_sub_state_type_ = MENU_CREDITS;
	}

	StateMenuCredits::~StateMenuCredits()
	{
	}

	void StateMenuCredits::SubStateInit(gef::Platform& platform, Camera* camera)
	{
		next_sub_state_type_ = MENU_CREDITS;

		// Init UI elements
		return_to_menu_text_.InitText(camera->position(), platform.width() * 0.5f, 490.f, "PRESS ESC TO RETURN", 1.f, 0xffffffff, gef::TextJustification::TJ_CENTRE);

		credits_text_[0].InitText(camera->position(), platform.width() * 0.5f, 150.f, "CREDITS", 1.f, 0xffffffff, gef::TextJustification::TJ_CENTRE);
		credits_text_[1].InitText(camera->position(), platform.width() * 0.5f - 350.f, 210.f, "Game Development by Ross McDonald", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);
		credits_text_[2].InitText(camera->position(), platform.width() * 0.5f - 350.f, 260.f, "Models & Textures by Ross McDonald", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);
		credits_text_[3].InitText(camera->position(), platform.width() * 0.5f - 350.f, 330.f, "This game was built using the Games", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);
		credits_text_[4].InitText(camera->position(), platform.width() * 0.5f - 350.f, 380.f, "Education Framework by Grant Clarke", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);

	}

	void StateMenuCredits::SubStateUpdate(gef::InputManager* input, float dt)
	{
		// check input
		if (input)
		{
			gef::Keyboard* keys = input->keyboard();
			
			// escape: return to menu
			if (keys->IsKeyPressed(keys->KC_ESCAPE))
			{
				next_sub_state_type_ = MENU_MAIN;
			}
		}
	}

	void StateMenuCredits::SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// render sub state
		// DEBUG: font->RenderText(sprite_renderer, gef::Vector4(250.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, "Credits");

		return_to_menu_text_.Render(sprite_renderer, font);

		for (int i = 0; i < MAX_CREDITS_TEXT; ++i)
		{
			credits_text_[i].Render(sprite_renderer, font);
		}

	}

	void StateMenuCredits::SubStateCleanUp()
	{}
}