#include "state_menu_main.h"

namespace s3d_app
{
	StateMenuMain::StateMenuMain()
	{
		sub_state_type_ = MENU_MAIN;
		next_sub_state_type_ = MENU_MAIN;
	}

	StateMenuMain::~StateMenuMain()
	{
	}

	void StateMenuMain::SubStateInit(gef::Platform& platform, Camera* camera)
	{
		next_sub_state_type_ = MENU_MAIN;

		// init menu
		menu_.InitTextMenu(camera->position(), platform.width()*0.5f, 300.f, 1.f, 40.f, 0xff77777777, 0xffffffff, gef::TJ_CENTRE);
		menu_.AddMenuItem("PLAY");
		menu_.AddMenuItem("SETTINGS");
		menu_.AddMenuItem("HELP");
		menu_.AddMenuItem("CREDITS");
		menu_.AddMenuItem("QUIT");
		menu_.SetSelected(MENU_MAIN);
	}

	void StateMenuMain::SubStateUpdate(gef::InputManager* input, float dt)
	{
		if (input)
		{
			gef::Keyboard* keys = input->keyboard();
			
			// Handle menu navigation:
			// if up pressed
			if (keys->IsKeyPressed(keys->KC_W))
			{
				menu_.SetSelected(menu_.selected() - 1);
			}
			// if down pressed
			if (keys->IsKeyPressed(keys->KC_S))
			{
				menu_.SetSelected(menu_.selected() + 1);
			}
			// if enter/return pressed
			if ((keys->IsKeyPressed(keys->KC_RETURN)) || (keys->IsKeyPressed(keys->KC_NUMPADENTER)))
			{
				switch (menu_.selected())
				{
				case PLAY:
					next_sub_state_type_ = PLAY_ACTIVE;
					break;
				case SETTINGS:
					next_sub_state_type_ = MENU_SETTINGS;
					break;
				case HELP:
					next_sub_state_type_ = MENU_HELP;
					break;
				case CREDITS:
					next_sub_state_type_ = MENU_CREDITS;
					break;
				case QUIT:
					next_sub_state_type_ = QUIT_GAME;
					break;
				}
			}
			// Escape to Quit
			if (keys->IsKeyPressed(keys->KC_ESCAPE))
			{
				next_sub_state_type_ = QUIT_GAME;
			}

		}
	}

	void StateMenuMain::SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// DEBUG: font->RenderText(sprite_renderer, gef::Vector4(250.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, "Main Menu");

		// main menu
		menu_.Render(sprite_renderer, font);
	}

	void StateMenuMain::SubStateCleanUp()
	{}

}
