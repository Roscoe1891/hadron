#include "state_play_pause.h"

namespace s3d_app
{
	StatePlayPause::StatePlayPause()
	{
		sub_state_type_ = PLAY_PAUSE;
		next_sub_state_type_ = sub_state_type_;
	}

	StatePlayPause::~StatePlayPause()
	{
	}

	void StatePlayPause::SubStateInit(gef::Platform& platform, Camera* camera)
	{
		next_sub_state_type_ = sub_state_type_;

		// init menu
		menu_.InitTextMenu(camera->position(), platform.width()*0.5f, 300.f, 1.f, 40.f, 0xff888888, 0xffffffff, gef::TJ_CENTRE);
		menu_.AddMenuItem("RESUME");
		menu_.AddMenuItem("QUIT");
		menu_.SetSelected(RESUME);
	}

	void StatePlayPause::SubStateUpdate(gef::InputManager* input, float dt)
	{
		if (input)
		{
			gef::Keyboard* keys = input->keyboard();

			// For players who expect menu options to be mapped to keys
			if (keys->IsKeyPressed(keys->KC_P))
			{
				next_sub_state_type_ = PLAY_ACTIVE;
			}
			if (keys->IsKeyPressed(keys->KC_ESCAPE))
			{
				next_sub_state_type_ = MENU_MAIN;
			}

			// Handle menu navigation:
			// if up pressed
			if (keys->IsKeyPressed(keys->KC_W))
			{
				menu_.SetSelected(menu_.selected() - 1);
			}
			// if down pressed
			if (keys->IsKeyPressed(keys->KC_S))
			{
				menu_.SetSelected(menu_.selected() + 1);
			}

			// if enter pressed
			if ((keys->IsKeyPressed(keys->KC_RETURN)) || (keys->IsKeyPressed(keys->KC_NUMPADENTER)))
			{
				switch (menu_.selected())
				{
				case RESUME:
					next_sub_state_type_ = PLAY_ACTIVE;
					break;
				case QUIT:
					next_sub_state_type_ = MENU_MAIN;
					break;
				}
			}
		}
	}

	void StatePlayPause::SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// render sub state
		// DEBUG font->RenderText(sprite_renderer, gef::Vector4(250.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, "Pause");
		menu_.Render(sprite_renderer, font);
	}

	void StatePlayPause::SubStateCleanUp()
	{}

}
