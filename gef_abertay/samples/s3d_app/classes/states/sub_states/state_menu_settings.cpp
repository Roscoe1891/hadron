#include "state_menu_settings.h"

namespace s3d_app
{
	StateMenuSettings::StateMenuSettings() : s3d_renderer_(nullptr)
	{
		sub_state_type_ = MENU_SETTINGS;
		next_sub_state_type_ = MENU_SETTINGS;
		s3d_effect_value_ = MAX_EFFECT_VALUES - 1;
	}

	StateMenuSettings::~StateMenuSettings()
	{
	}

	void StateMenuSettings::SettingsMenuStateInit(gef::Platform& platform, Camera* camera, S3DRenderer* s3d_renderer, GameData* game_data)
	{
		s3d_renderer_ = s3d_renderer;
		game_data_ = game_data;
		SubStateInit(platform, camera);
	}

	void StateMenuSettings::SubStateInit(gef::Platform& platform, Camera* camera)
	{
		next_sub_state_type_ = MENU_SETTINGS;

		// assign menu values
		AssignEffectValues();
		AssignDifficultyValues();

		// init UI elements
		heading_text_.InitText(camera->position(), platform.width() * 0.5f, 150.f, "SETTINGS", 1.f, 0xffffffff, gef::TextJustification::TJ_CENTRE);
		return_to_menu_text_.InitText(camera->position(), platform.width() * 0.5f, 490.f, "PRESS ESC TO RETURN", 1.f, 0xffffffff, gef::TextJustification::TJ_CENTRE);

		settings_menu_.InitTextMenu(camera->position(), platform.width()*0.5f, 210.f, 1.f, 60.f, 0xff77777777, 0xffffffff, gef::TJ_RIGHT);
		settings_menu_.AddMenuItem("3D Effect (+ | -):");
		settings_menu_.AddMenuItem("Difficulty (+ | -):");

		settings_menu_.SetSelected(S3D_EFFECT);
		settings_menu_values_.SetSelected(S3D_EFFECT);
		settings_menu_values_.InitTextMenu(camera->position(), platform.width()*0.5f + 120.f, 210.f, 1.f, 60.f, 0xff77777777, 0xffffffff, gef::TJ_CENTRE);
		settings_menu_values_.AddMenuItem("");
		settings_menu_values_.SetSelectedText(s3d_effect_value_);

		settings_menu_values_.AddMenuItem(difficulty_values_[difficulty_value_]);
		settings_menu_values_.SetSelected(DIFFICULTY);
		settings_menu_values_.SetSelectedText(difficulty_values_[difficulty_value_]);

		settings_menu_values_.SetSelected(S3D_EFFECT);
	}

	void StateMenuSettings::SubStateUpdate(gef::InputManager* input, float dt)
	{
		// check input
		if (input)
		{
			gef::Keyboard* keys = input->keyboard();

			// Escape: Quit to menu
			if (keys->IsKeyPressed(keys->KC_ESCAPE))
			{
				next_sub_state_type_ = MENU_MAIN;
			}

			// Handle menu navigation:
			// if up pressed
			if (keys->IsKeyPressed(keys->KC_W))
			{
				settings_menu_.SetSelected(settings_menu_.selected() - 1);
				settings_menu_values_.SetSelected(settings_menu_values_.selected() - 1);
			}
			// if down pressed
			if (keys->IsKeyPressed(keys->KC_S))
			{
				settings_menu_.SetSelected(settings_menu_.selected() + 1);
				settings_menu_values_.SetSelected(settings_menu_values_.selected() + 1);
			}

			// handle selection dependant input
			switch (settings_menu_.selected())
			{
				// Adjust 3D Effect
			case S3D_EFFECT:
				if ((keys->IsKeyPressed(keys->KC_NUMPADMINUS)) || (keys->IsKeyPressed(keys->KC_A)))
				{
					if (s3d_effect_value_ > 0)
					{
						s3d_effect_value_--;
						s3d_renderer_->set_eye_seperation_distance(s3d_effect_values_[s3d_effect_value_]);
						settings_menu_values_.SetSelectedText(s3d_effect_value_);
					}
				}
				if ((keys->IsKeyPressed(keys->KC_NUMPADPLUS)) || (keys->IsKeyPressed(keys->KC_D)))
				{
					if (s3d_effect_value_ < MAX_EFFECT_VALUES - 1)
					{
						s3d_effect_value_++;
						s3d_renderer_->set_eye_seperation_distance(s3d_effect_values_[s3d_effect_value_]);
						settings_menu_values_.SetSelectedText(s3d_effect_value_);
					}
				}
				break;
				// Adjust Difficulty
			case DIFFICULTY:
				if ((keys->IsKeyPressed(keys->KC_NUMPADMINUS)) || (keys->IsKeyPressed(keys->KC_A)))
				{
					switch (difficulty_value_)
					{
					case GameData::EASY:
						difficulty_value_ = GameData::HARD;
						break;
					case GameData::NORMAL:
						difficulty_value_ = GameData::EASY;
						break;
					case GameData::HARD:
						difficulty_value_ = GameData::NORMAL;
						break;
					}
					game_data_->set_difficulty(difficulty_value_);
					settings_menu_values_.SetSelectedText(difficulty_values_[difficulty_value_]);
				}
				if ((keys->IsKeyPressed(keys->KC_NUMPADPLUS)) || (keys->IsKeyPressed(keys->KC_D)))
				{
					switch (difficulty_value_)
					{
					case GameData::EASY:
						difficulty_value_ = GameData::NORMAL;
						break;
					case GameData::NORMAL:
						difficulty_value_ = GameData::HARD;
						break;
					case GameData::HARD:
						difficulty_value_ = GameData::EASY;
						break;
					}
					game_data_->set_difficulty(difficulty_value_);
					settings_menu_values_.SetSelectedText(difficulty_values_[difficulty_value_]);
				}
				break;
			default:
				break;
			}
		}
	}

	void StateMenuSettings::SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// render sub state
		// DEBUG: font->RenderText(sprite_renderer, gef::Vector4(250.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, "Settings");

		heading_text_.Render(sprite_renderer, font);
		return_to_menu_text_.Render(sprite_renderer, font);
		settings_menu_.Render(sprite_renderer, font);
		settings_menu_values_.Render(sprite_renderer, font);
	}

	void StateMenuSettings::SubStateCleanUp()
	{
	}

	void StateMenuSettings::AssignEffectValues()
	{
		s3d_effect_values_[0] = 0.f;
		s3d_effect_values_[1] = 0.004f;
		s3d_effect_values_[2] = 0.008f;
		s3d_effect_values_[3] = 0.012f;
		s3d_effect_values_[4] = 0.016f;
		s3d_effect_values_[5] = 0.02f;
		s3d_effect_values_[6] = 0.023f;
		s3d_effect_values_[7] = 0.026f;
		s3d_effect_values_[8] = 0.028f;
		s3d_effect_values_[9] = 0.03f;
		s3d_effect_values_[10] = 0.032f;

		for (int i = 0; i < MAX_EFFECT_VALUES; ++i)
		{
			if (s3d_renderer_->eye_seperation_distance() == s3d_effect_values_[i])
			{
				s3d_effect_value_ = i;
				break;
			}
		}

	}

	void StateMenuSettings::AssignDifficultyValues()
	{
		difficulty_values_[GameData::EASY] = "EASY";
		difficulty_values_[GameData::NORMAL] = "NORMAL";
		difficulty_values_[GameData::HARD] = "HARD";

		difficulty_value_ = game_data_->difficulty();
	}
}
