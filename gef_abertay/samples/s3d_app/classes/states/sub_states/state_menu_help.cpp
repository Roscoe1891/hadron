#include "state_menu_help.h"

namespace s3d_app
{
	StateMenuHelp::StateMenuHelp()
	{
		sub_state_type_ = MENU_HELP;
		next_sub_state_type_ = MENU_HELP;
	}

	StateMenuHelp::~StateMenuHelp()
	{
	}

	void StateMenuHelp::SubStateInit(gef::Platform& platform, Camera* camera)
	{
		next_sub_state_type_ = MENU_HELP;

		// Init UI
		return_to_menu_text_.InitText(camera->position(), platform.width() * 0.5f, 490.f, "PRESS ESC TO RETURN", 1.f, 0xffffffff, gef::TextJustification::TJ_CENTRE);

		controls_text_[0].InitText(camera->position(), platform.width() * 0.5f - 400.f, 125.f, "CONTROLS:", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);
		controls_text_[1].InitText(camera->position(), platform.width() * 0.5f - 400.f, 165.f, "A : Move Anti-Clockwise", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);
		controls_text_[2].InitText(camera->position(), platform.width() * 0.5f - 400.f, 205.f, "D : Move Clockwise", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);
		controls_text_[3].InitText(camera->position(), platform.width() * 0.5f - 400.f, 245.f, "S : Shoot", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);

		objective_text_[0].InitText(camera->position(), platform.width() * 0.5f - 400.f, 320.f, "OBJECTIVE:", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);
		objective_text_[1].InitText(camera->position(), platform.width() * 0.5f - 400.f, 360.f, "Shoot the advancing enemies & their missiles", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);
		objective_text_[2].InitText(camera->position(), platform.width() * 0.5f - 400.f, 400.f, "to stop them getting past you.", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);


	}

	void StateMenuHelp::SubStateUpdate(gef::InputManager* input, float dt)
	{
		// check input
		if (input)
		{
			gef::Keyboard* keys = input->keyboard();

			// escape: quit to main menu
			if (keys->IsKeyPressed(keys->KC_ESCAPE))
			{
				next_sub_state_type_ = MENU_MAIN;
			}
		}
	}

	void StateMenuHelp::SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// render sub state:

		// debug: font->RenderText(sprite_renderer, gef::Vector4(250.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, "Help");

		for (int i = 0; i < MAX_CONTROL_TEXT; ++i)
		{
			controls_text_[i].Render(sprite_renderer, font);
		}

		for (int i = 0; i < MAX_OBJECTIVE_TEXT; ++i)
		{
			objective_text_[i].Render(sprite_renderer, font);
		}

		return_to_menu_text_.Render(sprite_renderer, font);
	}

	void StateMenuHelp::SubStateCleanUp()
	{}
}
