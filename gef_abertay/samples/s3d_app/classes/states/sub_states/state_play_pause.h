#ifndef _STATE_PLAY_PAUSE_H
#define _STATE_PLAY_PAUSE_H

#include "sub_state.h"
#include "classes/gui/text_menu.h"

// Substate governing behaviour when the game is paused during play

namespace s3d_app
{
	class StatePlayPause : public SubState
	{
	public:
		// enumerate menu options
		enum PauseMenuOptions { RESUME, QUIT };

		StatePlayPause();
		~StatePlayPause();

		// From base class
		virtual void SubStateInit(gef::Platform& platform, Camera* camera);
		virtual void SubStateUpdate(gef::InputManager* input, float dt);
		virtual void SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void SubStateCleanUp();

	private:
		// The pause menu
		TextMenu menu_;

	};
}

#endif