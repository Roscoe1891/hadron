#ifndef _STATE_MENU_SETTINGS_H
#define _STATE_MENU_SETTINGS_H

#include "sub_state.h"
#include <classes/gui/text_menu.h>
#include <classes/graphics/s3d_renderer.h>

namespace s3d_app
{
// maximum number of s3d effect values
#define MAX_EFFECT_VALUES 11

	class StateMenuSettings : public SubState
	{
	public:
		// Enumerate menu options
		enum SettingsMenuOptions { S3D_EFFECT, DIFFICULTY };

		StateMenuSettings();
		~StateMenuSettings();

		// From base class
		virtual void SubStateInit(gef::Platform& platform, Camera* camera);
		virtual void SubStateUpdate(gef::InputManager* input, float dt);
		virtual void SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void SubStateCleanUp();

		// Substate-specific Init function for the Setting substate
		void SettingsMenuStateInit(gef::Platform& platform, Camera* camera, S3DRenderer* s3d_renderer, GameData* game_data);

	private:
		// Assign the values for the 3D effects
		void AssignEffectValues();
		// Assign the difficulty values
		void AssignDifficultyValues();

		// pointer to the s3d renderer
		S3DRenderer* s3d_renderer_;
		// pointer to game data
		GameData* game_data_;

		// GUI elements
		Text return_to_menu_text_;
		Text heading_text_;
		TextMenu settings_menu_;
		TextMenu settings_menu_values_;

		// array used to store S3D effect values
		float s3d_effect_values_[MAX_EFFECT_VALUES];
		// selected s3d effect value
		int s3d_effect_value_;

		// array used to store difficulty values
		std::string difficulty_values_[GameData::MAX_DIFFICULTIES];
		// selected difficulty value
		GameData::Difficulty difficulty_value_;


	};
}

#endif
