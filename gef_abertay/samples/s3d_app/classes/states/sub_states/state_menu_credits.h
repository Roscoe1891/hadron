#ifndef _STATE_MENU_CREDITS_H
#define _STATE_MENU_CREDITS_H

#include "sub_state.h"

namespace s3d_app
{
#define MAX_CREDITS_TEXT 5

	class StateMenuCredits : public SubState
	{
	public:
		StateMenuCredits();
		~StateMenuCredits();

		// Initialise substate
		virtual void SubStateInit(gef::Platform& platform, Camera* camera);
		virtual void SubStateUpdate(gef::InputManager* input, float dt);
		virtual void SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void SubStateCleanUp();

	private:
		// UI elements
		Text return_to_menu_text_;
		Text credits_text_[MAX_CREDITS_TEXT];
	};
}

#endif
