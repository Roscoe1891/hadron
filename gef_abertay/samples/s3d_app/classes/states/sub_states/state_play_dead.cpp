#include "state_play_dead.h"

namespace s3d_app
{
	StatePlayDead::StatePlayDead()
	{
		sub_state_type_ = PLAY_DEAD;
		next_sub_state_type_ = sub_state_type_;
	}

	StatePlayDead::~StatePlayDead()
	{
	}

	void StatePlayDead::SubStateInit(gef::Platform& platform, Camera* camera)
	{
		next_sub_state_type_ = sub_state_type_;

		// init menu
		menu_.InitTextMenu(camera->position(), platform.width()*0.5f, 300.f, 1.f, 40.f, 0xff888888, 0xffffffff, gef::TJ_CENTRE);
		menu_.AddMenuItem("REPLAY");
		menu_.AddMenuItem("QUIT");
		menu_.SetSelected(REPLAY);

	}

	void StatePlayDead::SubStateUpdate(gef::InputManager* input, float dt)
	{
		// check input
		if (input)
		{
			gef::Keyboard* keys = input->keyboard();

			// For players who expect menu options to be mapped to keys
			if (keys->IsKeyPressed(keys->KC_R))
			{
				next_sub_state_type_ = PLAY_ACTIVE;
			}
			if (keys->IsKeyPressed(keys->KC_ESCAPE))
			{
				next_sub_state_type_ = MENU_MAIN;
			}

			// Handle menu navigation:
			// if up pressed
			if (keys->IsKeyPressed(keys->KC_W))
			{
				menu_.SetSelected(menu_.selected() - 1);
			}
			// if down pressed
			if (keys->IsKeyPressed(keys->KC_S))
			{
				menu_.SetSelected(menu_.selected() + 1);
			}
			
			// if enter pressed / return pressed
			if ((keys->IsKeyPressed(keys->KC_RETURN)) || (keys->IsKeyPressed(keys->KC_NUMPADENTER)))
			{
				switch (menu_.selected())
				{
				case REPLAY:
					next_sub_state_type_ = PLAY_ACTIVE;
					break;
				case QUIT:
					next_sub_state_type_ = MENU_MAIN;
					break;
				}
			}

		}
	}

	void StatePlayDead::SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// render substate
		// DEBUG font->RenderText(sprite_renderer, gef::Vector4(250.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, "Dead");
		menu_.Render(sprite_renderer, font);
	}

	void StatePlayDead::SubStateCleanUp()
	{}
}
