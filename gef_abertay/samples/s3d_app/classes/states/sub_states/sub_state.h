#ifndef _SUB_STATE_H
#define _SUB_STATE_H

#include <classes/graphics/camera.h>
#include <classes/gui/text.h>
#include <input/input_manager.h>
#include <input/keyboard.h>
#include <graphics/sprite_renderer.h>
#include <graphics/font.h>
#include <platform/d3d11/system/platform_d3d11.h>
#include <string>

// Is a sub state is a state which is implemented by another state
// The current sub state's functions should be called by the parent state, in addition to common parent state functions

namespace s3d_app
{
	class SubState
	{
	public:
		// Enumerate sub state types
		enum SubStateType { MENU_MAIN, MENU_SETTINGS, MENU_HELP, MENU_CREDITS, NO_MENU_SUB_STATES, PLAY_ACTIVE, PLAY_PAUSE, PLAY_DEAD, NO_PLAY_SUB_STATES, QUIT_GAME };

		SubState();
		virtual~SubState();

		// Initialise substate
		virtual void SubStateInit(gef::Platform& platform, Camera* camera) = 0;
		// Update substate
		virtual void SubStateUpdate(gef::InputManager* input, float dt) = 0;
		// Render substate
		virtual void SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font) = 0;
		// Clean up substate
		virtual void SubStateCleanUp() = 0;

		// get sub state type
		inline const SubStateType sub_state_type() { return sub_state_type_; }
		// get next substate type
		inline const SubStateType next_sub_state_type() { return next_sub_state_type_; }

	protected:
		// this sub state's type
		SubStateType sub_state_type_;
		// the next sub state type
		SubStateType next_sub_state_type_;
	};
}

#endif // !_SUB_STATE_H
