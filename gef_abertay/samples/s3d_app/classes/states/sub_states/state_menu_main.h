#ifndef _STATE_MENU_MAIN_H
#define _STATE_MENU_MAIN_H

#include "sub_state.h"
#include "classes/gui/text_menu.h"
#include "classes/states/state.h"

namespace s3d_app
{
	class StateMenuMain : public SubState
	{
	public:
		// enumerate menu options
		enum MENU_OPTIONS { PLAY, SETTINGS, HELP, CREDITS, QUIT};

		StateMenuMain();
		~StateMenuMain();

		// From base class
		virtual void SubStateInit(gef::Platform& platform, Camera* camera);
		virtual void SubStateUpdate(gef::InputManager* input, float dt);
		virtual void SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void SubStateCleanUp();

	private:
		TextMenu menu_;

	};
}

#endif