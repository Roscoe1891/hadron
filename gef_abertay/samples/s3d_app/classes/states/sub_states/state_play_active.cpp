#include "state_play_active.h"

namespace s3d_app
{
	StatePlayActive::StatePlayActive()
	{
		sub_state_type_ = PLAY_ACTIVE;
		next_sub_state_type_ = sub_state_type_;
	}

	StatePlayActive::~StatePlayActive()
	{
	}

	void StatePlayActive::SubStateInit(gef::Platform& platform, Camera* camera)
	{
		next_sub_state_type_ = sub_state_type_;

		// init counters
		score_counter_.InitCounter(camera->position(), 10.f, 10.f, "SCORE: ", 1.f, 0xffffffff, gef::TextJustification::TJ_LEFT);
		lives_counter_.InitCounter(camera->position(), platform.width() - 10.f, 10.f, "LIVES: ", 1.f, 0xffffffff, gef::TextJustification::TJ_RIGHT);
	}

	void StatePlayActive::ActivePlayStateInit(gef::Platform& platform, Camera* camera, GameData* game_data)
	{
		game_data_ = game_data;
		SubStateInit(platform, camera);
	}

	void StatePlayActive::ActivePlayStateReset(Player* player, EnemyManager* enemy_manager)
	{
		next_sub_state_type_ = sub_state_type_;
	
		// reset player
		player->ResetPlayer();
		// reset enemy manager
		enemy_manager->ResetEnemies();

		// update ui
		UpdateGUI(player);
	}

	void StatePlayActive::ResumeFromPause()
	{
		next_sub_state_type_ = sub_state_type_;
	}


	void StatePlayActive::ActivePlayStateUpdate(gef::InputManager* input, float dt, Player* player, EnemyManager* enemy_manager, Bounds* bounds)
	{
		if (input)
		{
			if (player->live())
			{
				// sub-state update
				SubStateUpdate(input, dt);

				// update player
				Player::ScoreUpdate score_update;
				player->UpdatePlayer(dt, input->keyboard());

				// update enemy manager
				enemy_manager->Update(dt, &physics_);

				// Run Collision Checks
				CollisionChecks(player, enemy_manager, bounds);
				// Apply Collision Reponses
				CollisionResponses(player, enemy_manager);
			}
			else
			{
				next_sub_state_type_ = PLAY_DEAD;
			}
		}
	}

	void StatePlayActive::SubStateUpdate(gef::InputManager* input, float dt)
	{
			gef::Keyboard* keys = input->keyboard();

			// PAUSE
			if (keys->IsKeyPressed(keys->KC_P))
			{
				next_sub_state_type_ = PLAY_PAUSE;
			}
	}

	void StatePlayActive::SubStateRender(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// render substate

		// DEBUG: font->RenderText(sprite_renderer, gef::Vector4(250.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, "Active");

		// draw lives
		lives_counter_.Render(sprite_renderer, font);
		// draw score
		score_counter_.Render(sprite_renderer, font);

	}

	void StatePlayActive::CollisionChecks(Player* player, EnemyManager* enemy_manager, Bounds* bounds)
	{
		// check enemies
		for (int enemy = 0; enemy < MAX_ENEMIES; ++enemy)
		{
			// if live
			if (enemy_manager->enemies()[enemy].live())
			{
				// check enemy vs player
				physics_.CheckCollision(&enemy_manager->enemies()[enemy], player, Collision::SPHERE);

				// check enemy vs NEAR bounds
				physics_.CheckCollision(&enemy_manager->enemies()[enemy], &bounds[Bounds::NEAR_BOUNDS], Collision::BOUNDS);
				
				// check enemy vs player projectiles
				for (int player_projectile = 0; player_projectile < MAX_AMMO; ++player_projectile)
				{
					// if live
					if (player->projectiles()[player_projectile].live())
						// do check
						physics_.CheckCollision(&enemy_manager->enemies()[enemy], &player->projectiles()[player_projectile], Collision::SPHERE);
				}

				// check enemy's projectiles
				for (int enemy_projectile = 0; enemy_projectile < MAX_AMMO; ++enemy_projectile)
				{
					// if live
					if (enemy_manager->enemies()[enemy].projectiles()[enemy_projectile].live())
					{
						// check enemy's projectiles vs player
						physics_.CheckCollision(&enemy_manager->enemies()[enemy].projectiles()[enemy_projectile], player, Collision::SPHERE);

						// check enemy's projectiles vs player projectiles
						for (int player_projectile = 0; player_projectile < MAX_AMMO; ++player_projectile)
						{
							// if live
							if (player->projectiles()[player_projectile].live())
								// do check
								physics_.CheckCollision(&enemy_manager->enemies()[enemy].projectiles()[enemy_projectile], &player->projectiles()[player_projectile], Collision::SPHERE);
						}

						// check enemy projectile vs NEAR bounds
						physics_.CheckCollision(&enemy_manager->enemies()[enemy].projectiles()[enemy_projectile], &bounds[Bounds::NEAR_BOUNDS], Collision::BOUNDS);
					}
				}
			}
		}

		// check player projectile vs FAR bounds
		for (int player_projectile = 0; player_projectile < MAX_AMMO; ++player_projectile)
		{
			// if live
			if (player->projectiles()[player_projectile].live())
			{
				// do check
				physics_.CheckCollision(&player->projectiles()[player_projectile], &bounds[Bounds::FAR_BOUNDS], Collision::BOUNDS);
			}
		}
	}

	void StatePlayActive::CollisionResponses(Player* player, EnemyManager* enemy_manager)
	{
		// iterate through collision list
		for (auto collisions_iter = physics_.GetCollisionListStart(); collisions_iter != physics_.GetCollisionListEnd(); ++collisions_iter)
		{
			// call on collision for both objects
			(*collisions_iter)->gameObject_1()->OnCollision((*collisions_iter)->gameObject_2()->go_type());
			(*collisions_iter)->gameObject_2()->OnCollision((*collisions_iter)->gameObject_1()->go_type());

			// notify enemy manager of collision
			enemy_manager->CollisionNotify((*collisions_iter)->gameObject_1(), (*collisions_iter)->gameObject_2());

			// prepare and send score update to player
			SendPlayerScoreUpdate(player, enemy_manager);

			// Update UI
			UpdateGUI(player);
		}
			
		// Clear collision list
		physics_.ClearCollisionList();
	}

	// prepare and send score update to player
	void StatePlayActive::SendPlayerScoreUpdate(Player* player, EnemyManager* enemy_manager)
	{
		// get enemy score data from enemy manager
		EnemyManager::EnemyScoreData enemy_scores = enemy_manager->enemy_score_data();
		// create update
		Player::ScoreUpdate player_update = CreatePlayerScoreUpdate(enemy_scores);
		// send
		player->UpdateScore(player_update);
	}

	Player::ScoreUpdate StatePlayActive::CreatePlayerScoreUpdate(EnemyManager::EnemyScoreData enemy_score_data)
	{
		// Create a player score update
		Player::ScoreUpdate score_update;
		// Populate from enemy score data
		score_update.breached_enemies = enemy_score_data.enemies_breached;
		score_update.breached_projectiles = enemy_score_data.enemy_projectiles_breached;
		score_update.destroyed_enemies = enemy_score_data.enemies_killed_by_player;
		score_update.destroyed_projectiles = enemy_score_data.enemy_projectiles_killed_by_player;

		return score_update;
		
	}

	// Update UI
	void StatePlayActive::UpdateGUI(Player* player)
	{
		score_counter_.UpdateCounter(player->score());
		lives_counter_.UpdateCounter(player->lives());
	}

	void StatePlayActive::SubStateCleanUp()
	{
	}
}
