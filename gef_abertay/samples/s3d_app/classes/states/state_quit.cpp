#include "state_quit.h"

namespace s3d_app
{
	StateQuit::StateQuit() : mesh_data_manager_(nullptr)
	{
		state_type_ = QUIT;
		next_state_type_ = state_type_;

		state_text_ = "Quit State.";
	}

	StateQuit::~StateQuit()
	{
	}

	void StateQuit::StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		StateInit(platform, camera, game_data, mesh_data_manager);
	}

	void StateQuit::StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		next_state_type_ = state_type_;
		mesh_data_manager_ = mesh_data_manager;
		game_data_ = game_data;
	}
	
	bool StateQuit::StateUpdate(gef::InputManager* input, float dt)
	{
		// unload assets
		Unload();

		return false;
	}
	
	void StateQuit::StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// DEBUG: font->RenderText(sprite_renderer, gef::Vector4(10.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, state_text_.c_str());
	}

	void StateQuit::StateRender3D(gef::Renderer3D* renderer_3d)
	{
	}

	void StateQuit::StateCleanUp()
	{
	}

	void StateQuit::Unload()
	{
		mesh_data_manager_->CleanUp();
	}
}