#ifndef _STATE_QUIT_H
#define _STATE_QUIT_H

#include "state.h"

// state to define behaviour immediately prior to app exit
// should be used to unload / cleanup

namespace s3d_app
{
	class StateQuit : public State
	{
	public:
		StateQuit();
		~StateQuit();

		// From base state
		virtual void StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual void StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual bool StateUpdate(gef::InputManager* input, float dt);
		virtual void StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void StateRender3D(gef::Renderer3D* renderer_3d);
		virtual void StateCleanUp();

	private:
		// Unload assets
		void Unload();

		// Pointer to the mesh data manager
		MeshDataManager* mesh_data_manager_;
	};
}

#endif

