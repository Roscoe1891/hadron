#ifndef _STATE_GAME_OVER_H
#define _STATE_GAME_OVER_H

#include "state.h"
#include <classes/gui/text.h>

// state defining behaviour during the game over screen

namespace s3d_app
{
	class StateGameOver : public State
	{
	public:
		StateGameOver();
		~StateGameOver();

		// From base state
		virtual void StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual void StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual bool StateUpdate(gef::InputManager* input, float dt);
		virtual void StateRender3D(gef::Renderer3D* renderer_3d);
		virtual void StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void StateCleanUp();

	private:
		// UI elements
		Text header_text_;
		Text sub_header_text_;
		Text score_text_;
		Text return_to_menu_text_;
	};

	
}

#endif // !_STATE_GAME_OVER_H

