#include "state_manager.h"

namespace s3d_app
{
	StateManager::StateManager() :
		platform_(nullptr),
		camera_(nullptr),
		s3d_renderer_(nullptr),
		game_data_(nullptr)
	{
		AssignStates();
		current_state_ = State::START;
	}

	StateManager::~StateManager()
	{
	}

	void StateManager::Init(gef::Platform* platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		// assign values
		platform_ = platform;
		s3d_renderer_ = s3d_renderer;
		camera_ = camera;
		game_data_ = game_data;
		mesh_data_manager_ = mesh_data_manager;

		// set current state
		states_[current_state_]->StateInit(*platform_, camera_, game_data_, mesh_data_manager);
	}

	bool StateManager::Update(gef::InputManager* input, float dt)
	{
		// Update current state
		if (!states_[current_state_]->StateUpdate(input, dt))
			return false;

		// check for state transition
		if (states_[current_state_]->next_state_type() != current_state_)
		{
			ChangeState();
		}

		return true;
	}

	void StateManager::RenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// render curretn state UI
		if (sprite_renderer && font)
		states_[current_state_]->StateRenderUI(sprite_renderer, font);
	}

	void StateManager::AssignStates()
	{
		states_[State::START] = &start_state_;
		states_[State::MENU] = &menu_state_;
		states_[State::PLAY] = &play_state_;
		states_[State::GAME_OVER] = &game_over_state_;
		states_[State::QUIT] = &quit_state_;
	}

	void StateManager::ChangeState()
	{
		// clean up current state
		states_[current_state_]->StateCleanUp();
		// set current state to next state
		current_state_ = states_[current_state_]->next_state_type();

		// check for state specific initialisation
		switch (current_state_)
		{
		case State::MENU:
			states_[current_state_]->StateInit(*platform_, s3d_renderer_, camera_, game_data_, mesh_data_manager_);
			break;
		default:
			states_[current_state_]->StateInit(*platform_, camera_, game_data_, mesh_data_manager_);
			break;

		}
	}

	State* StateManager::GetCurrentState() const
	{
		return states_[current_state_];
	}

}