#ifndef _STATE_START_H
#define _STATE_START_H

#include "state.h"
#include <classes/gui/text.h>

// State defining behaviour for the app start up
// Loads in assets before exiting to next state

namespace s3d_app
{
	class StateStart : public State
	{
	public:
		StateStart();
		~StateStart();

		// From base state
		virtual void StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual void StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual bool StateUpdate(gef::InputManager* input, float dt);
		virtual void StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void StateRender3D(gef::Renderer3D* renderer_3d);
		virtual void StateCleanUp();

		// Load Assets 
		void LoadAssets(gef::Platform& platform, MeshDataManager* mesh_data_manager);

	private:
		// true if all assets have loaded
		bool loaded_;

		// UI elements
		Text header_text_;
		Text progress_text_;

	};

}
#endif // !_STATE_START_H

