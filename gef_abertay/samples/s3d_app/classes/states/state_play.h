#ifndef _STATE_PLAY_H
#define _STATE_PLAY_H

#include "state.h"
#include <assets/obj_loader.h>
#include <classes/game_object/player.h>
#include <classes/game_object/enemy_manager.h>
#include <classes/game_object/bounds.h>
#include <classes/level/level_manager.h>
#include <classes/states/sub_states/state_play_active.h>
#include <classes/states/sub_states/state_play_pause.h>
#include <classes/states/sub_states/state_play_dead.h>
#include <graphics/model.h>

// State defining behaviour during play
// Includes Sub states: Active, Pause and Dead

namespace s3d_app
{

// An offset is required to make use of substate enums as array indices
#define SUB_STATE_PLAY_ENUM_OFFSET SubState::NO_MENU_SUB_STATES - 1

	class StatePlay : public State
	{
	public:
		StatePlay();
		~StatePlay();

		// From base state
		virtual void StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual void StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual bool StateUpdate(gef::InputManager* input, float dt);
		virtual void StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void StateRender3D(gef::Renderer3D* renderer_3d);
		virtual void StateCleanUp();

	private:
		// intialise the playing bounds
		void InitBounds();
		// assign substates to substates array
		void AssignSubStates();
		// change to a new sub state
		void ChangeSubState();
		// return the current play substate index
		int GetCurrentSubStateIndex();
		// re-initilaise for next level
		void NextLevel();

		// the current sub state
		SubState::SubStateType current_sub_state_;
		// array of pointers to the play sub states
		SubState* play_sub_states_[SubState::NO_PLAY_SUB_STATES - SUB_STATE_PLAY_ENUM_OFFSET];

		// The Active Play State
		StatePlayActive active_play_state_;
		// The Pause Play State
		StatePlayPause pause_play_state_;
		// The Dead Play State
		StatePlayDead dead_play_state_;

		// Pointer to platform
		gef::Platform* platform_;
		// Pointer to camera
		Camera* camera_;
		// Pointer to mesh data manager
		MeshDataManager* mesh_data_manager_;
		
		// The Level Manager
		LevelManager level_manager_;

		// Boundaries of the play boundaries
		Bounds bounds_[Bounds::MAX_BOUNDS];

		// The Enemy Manager
		EnemyManager enemy_manager_;

		// The player
		gef::Model player_model_;
		Player player_;

		// Counter to display current level number
		Counter level_counter_;

	};
}

#endif
