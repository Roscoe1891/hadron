#include "state_menu.h"

namespace s3d_app
{
	StateMenu::StateMenu() : k_projectile_spawn_delay_(0.25f), k_projectile_origin_(gef::Vector4(0.f, 0.f, -100.f)), k_projectile_velocity_(gef::Vector4(0.f, 0.f, 15.f)),
		k_projectile_XY_max_(30), k_projectile_XY_min_(-30), s3d_renderer_(nullptr)
	{
		state_type_ = MENU;
		next_state_type_ = state_type_;

		current_sub_state_ = SubState::MENU_MAIN;

		state_text_ = "Menu State.";

		projectile_spawn_timer_ = 0.f;

		AssignSubStates();
	}

	StateMenu::~StateMenu()
	{
	}

	void StateMenu::StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		next_state_type_ = state_type_;
		current_sub_state_ = SubState::MENU_MAIN;
		menu_sub_states_[current_sub_state_]->SubStateInit(platform, camera);

		platform_ = &platform;
		camera_ = camera;
		game_data_ = game_data;

		// Init UI
		header_text_.InitText(camera_->position(), platform_->width()*0.5f, 50.f, "H A D R O N", 1.f, 0xffffffff, gef::TJ_CENTRE);

		// Init Display projectiles
		for (int i = 0; i < MAX_PROJECTILES; ++i)
		{
			gef::Vector4 start_pos = GetRandomPosition_XYZ();
			projectiles_[i].Init(mesh_data_manager->scene_meshes()[MeshDataManager::PROJECTILE_MESH], start_pos, k_projectile_velocity_);
			projectiles_[i].set_live(true);
		}

		projectile_spawn_timer_ = 0.f;

	}

	void StateMenu::StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		s3d_renderer_ = s3d_renderer;
		StateInit(platform, camera, game_data, mesh_data_manager);
	}



	bool StateMenu::StateUpdate(gef::InputManager* input, float dt)
	{
		/*if (input)
		{
			gef::Keyboard* keys = input->keyboard();

			if (keys->IsKeyPressed(keys->KC_F))
			{
				next_state_type_ = PLAY;
			}
		}*/

		// update current sub state
		menu_sub_states_[current_sub_state_]->SubStateUpdate(input, dt);

		// check for spawning new projectiles
		if (projectile_spawn_timer_ < k_projectile_spawn_delay_)
		{
			projectile_spawn_timer_ += dt;
		}
		else
		{
			SpawnProjectile();
		}

		// update the projectile display
		for (int i = 0; i < MAX_PROJECTILES; ++i)
		{
			if (projectiles_[i].live())
			{
				projectiles_[i].Update(dt);

				// reset projectiles on passing camera
				if (projectiles_[i].position().z() > camera_->position().z())
				{
					projectiles_[i].Die(GameObject::NO_TYPE);
				}
			}
		}

		// check for sub-state changes
		if (menu_sub_states_[current_sub_state_]->next_sub_state_type() != current_sub_state_)
		{
			ChangeSubState();
		}

		return true;
	}

	void StateMenu::StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// render state UI

		// DEBUG: font->RenderText(sprite_renderer, gef::Vector4(10.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, state_text_.c_str());

		header_text_.Render(sprite_renderer, font);
		menu_sub_states_[current_sub_state_]->SubStateRender(sprite_renderer, font);

	}

	void StateMenu::StateRender3D(gef::Renderer3D* renderer_3d)
	{
		// render s3d objects
		for (int i = 0; i < MAX_PROJECTILES; ++i)
		{
			if (projectiles_[i].live()) projectiles_[i].Render(renderer_3d);
		}
	}

	void StateMenu::StateCleanUp()
	{}

	void StateMenu::AssignSubStates()
	{
		menu_sub_states_[SubState::MENU_MAIN] = &main_menu_state_;
		menu_sub_states_[SubState::MENU_SETTINGS] = &settings_menu_state_;
		menu_sub_states_[SubState::MENU_HELP] = &help_menu_state_;
		menu_sub_states_[SubState::MENU_CREDITS] = &credits_menu_state_;
	}

	void StateMenu::ChangeSubState()
	{
		// clean up current sub state
		menu_sub_states_[current_sub_state_]->SubStateCleanUp();
		// set current sub state to next sub state
		current_sub_state_ = menu_sub_states_[current_sub_state_]->next_sub_state_type();

		// Check for state exit
		switch (current_sub_state_)
		{
		case SubState::PLAY_ACTIVE:
			next_state_type_ = PLAY;
			break;
		case SubState::QUIT_GAME:
			next_state_type_ = QUIT;
			break;
		case SubState::MENU_SETTINGS:
			static_cast<StateMenuSettings*>(menu_sub_states_[current_sub_state_])->SettingsMenuStateInit(*platform_, camera_, s3d_renderer_, game_data_);
			break;
		default:
			menu_sub_states_[current_sub_state_]->SubStateInit(*platform_, camera_);
		}

	}

	void StateMenu::SpawnProjectile()
	{
		// Spawn a projectile at the projectile origin 
		for (int i = 0; i < MAX_PROJECTILES; ++i)
		{
			if (!projectiles_[i].live())
			{
				gef::Vector2 spawn_pos = GetRandomPosition_XY();
				gef::Vector4 pos = gef::Vector4(spawn_pos.x, spawn_pos.y, k_projectile_origin_.z());
				projectiles_[i].Fire(pos);
				projectile_spawn_timer_ = 0.f;
				break;
			}
		}
	}

	gef::Vector2 StateMenu::GetRandomPosition_XY()
	{
		// Return a randomised XY co-ordinates for spawning projectiles for the display

		float x = rng_.RandomRange(k_projectile_XY_min_, k_projectile_XY_max_);
		float y = rng_.RandomRange(k_projectile_XY_min_, k_projectile_XY_max_);

		gef::Vector2 result = gef::Vector2(x, y);

		return result;
	}


	gef::Vector4 StateMenu::GetRandomPosition_XYZ()
	{
		// Return a randomised XYZ co-ordinates for spawning projectiles for the display

		float x = rng_.RandomRange(k_projectile_XY_min_, k_projectile_XY_max_);
		float y = rng_.RandomRange(k_projectile_XY_min_, k_projectile_XY_max_);
		float z = rng_.RandomRange(k_projectile_origin_.z(), camera_->position().z());

		gef::Vector4 result = gef::Vector4(x, y, z);

		return result;
	}
}