#include "state_game_over.h"

namespace s3d_app
{
	StateGameOver::StateGameOver()
	{
		state_type_ = GAME_OVER;
		next_state_type_ = state_type_;

		state_text_ = "Game Over State.";
	}

	StateGameOver::~StateGameOver()
	{
	}

	void StateGameOver::StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		next_state_type_ = state_type_;
		game_data_ = game_data;

		// init UI elementss
		header_text_.InitText(camera->position(), platform.width() * 0.5, platform.height() * 0.5 -100.f, "WELL DONE!", 1.f, 0xffffffff, gef::TJ_CENTRE);
		sub_header_text_.InitText(camera->position(), platform.width() * 0.5, platform.height() * 0.5f -50.f, "Final Score:", 1.f, 0xffffffff, gef::TJ_CENTRE);
		score_text_.InitText(camera->position(), platform.width() * 0.5, platform.height() * 0.5, "Score", 1.f, 0xffffffff, gef::TJ_CENTRE);
		return_to_menu_text_.InitText(camera->position(), platform.width() * 0.5f, 490.f, "PRESS ESC TO RETURN", 1.f, 0xffffffff, gef::TextJustification::TJ_CENTRE);
		score_text_.set_text_from_int(game_data_->final_score());
	}

	void StateGameOver::StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		StateInit(platform, camera, game_data, mesh_data_manager);
	}

	
	bool StateGameOver::StateUpdate(gef::InputManager* input, float dt)
	{
		if (input)
		{
			gef::Keyboard* keys = input->keyboard();

			// Escape or return to get back to menu
			if ((keys->IsKeyPressed(keys->KC_ESCAPE)) || (keys->IsKeyPressed(keys->KC_RETURN)))
			{
				next_state_type_ = MENU;
			}
		}

		return true;
	}
		
	void StateGameOver::StateRender3D(gef::Renderer3D* renderer_3d)
	{
	}

	void StateGameOver::StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// Render state ui

		// DEBUG: font->RenderText(sprite_renderer, gef::Vector4(10.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, state_text_.c_str());

		header_text_.Render(sprite_renderer, font);
		sub_header_text_.Render(sprite_renderer, font);
		score_text_.Render(sprite_renderer, font);
		return_to_menu_text_.Render(sprite_renderer, font);
	}
	
	void StateGameOver::StateCleanUp()
	{
		game_data_->set_final_score(0);
	}
}