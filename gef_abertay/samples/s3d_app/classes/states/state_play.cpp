#include "state_play.h"

namespace s3d_app
{
	StatePlay::StatePlay() : platform_(nullptr), mesh_data_manager_(nullptr)
	{
		state_type_ = PLAY;
		next_state_type_ = state_type_;

		current_sub_state_ = SubState::PLAY_ACTIVE;

		state_text_ = "Play State.";

		AssignSubStates();
	}

	StatePlay::~StatePlay()
	{
	}

	void StatePlay::StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		StateInit(platform, camera, game_data, mesh_data_manager);
	}

	void StatePlay::StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager)
	{
		next_state_type_ = state_type_;

		// assign init values
		platform_ = &platform;
		camera_ = camera;
		game_data_ = game_data;
		mesh_data_manager_ = mesh_data_manager;

		// Init Level
		level_manager_.InitLevel(mesh_data_manager, 0);

		// Init Bounds
		InitBounds();

		// Init Enemy Manager
		enemy_manager_.Init(mesh_data_manager, level_manager_.current_level().level_faces(), 
			level_manager_.current_level().level_geometry_type(), 8);

		// Init Player
		player_.InitPlayer(mesh_data_manager->scene_meshes()[MeshDataManager::PLAYER_MESH],
			mesh_data_manager->scene_meshes()[MeshDataManager::SHADOW_IMPOSTER_MESH],
			game_data_, level_manager_.current_level().level_faces(), level_manager_.current_level().bottom_face(), 
			level_manager_.current_level().level_geometry_type(), 8, 60);

		// Init level counter
		level_counter_.InitCounter(camera->position(), platform.width() * 0.5f, 10.f, "LEVEL: ", 1.f, 0xffffffff, gef::TextJustification::TJ_CENTRE);
		level_counter_.UpdateCounter(level_manager_.current_level().level_number() + 1);

		// Init State
		current_sub_state_ = SubState::PLAY_ACTIVE;
		static_cast<StatePlayActive*>(play_sub_states_[GetCurrentSubStateIndex()])->ActivePlayStateInit(platform, camera_, game_data_);
		static_cast<StatePlayActive*>(play_sub_states_[GetCurrentSubStateIndex()])->ActivePlayStateReset(&player_, &enemy_manager_);
	}

	
	void StatePlay::InitBounds()
	{
		gef::Mesh* null_mesh = nullptr;

		// init near bounds
		bounds_[Bounds::NEAR_BOUNDS].set_bounds_type(Bounds::NEAR_BOUNDS);
		bounds_[Bounds::NEAR_BOUNDS].Init(null_mesh, gef::Vector4(0.f, 0.f, NEAR_BOUNDS_Z), gef::Vector4(0.f, 0.f, 0.f));

		// init far bounds
		bounds_[Bounds::FAR_BOUNDS].set_bounds_type(Bounds::FAR_BOUNDS);
		bounds_[Bounds::FAR_BOUNDS].Init(null_mesh, gef::Vector4(0.f, 0.f, FAR_BOUNDS_Z), gef::Vector4(0.f, 0.f, 0.f));
	}

	bool StatePlay::StateUpdate(gef::InputManager* input, float dt)
	{
		//// check input
		//if (input)
		//{
		//	gef::Keyboard* keys = input->keyboard();

		//	// auto-win: Press F
		//	if (keys->IsKeyPressed(keys->KC_F))
		//	{
		//		next_state_type_ = GAME_OVER;
		//	}
		//}

		// update sub state
		switch (current_sub_state_)
		{
		case SubState::PLAY_ACTIVE:
			static_cast<StatePlayActive*>(play_sub_states_[GetCurrentSubStateIndex()])->ActivePlayStateUpdate(input, dt, &player_, &enemy_manager_, bounds_);
			// Check if level has ended
			if (enemy_manager_.enemy_kill_count() >= level_manager_.current_level().enemies_this_level())
			{
				// Check if this is the last level
				if (level_manager_.OnLastLevel())
				{
					next_state_type_ = GAME_OVER;
					// record final score
					game_data_->set_final_score(player_.score());
				}
				else
				{
					// Advance to next level
					NextLevel();
				}
			}
			break;
		default:
			// standard sub state update
			play_sub_states_[GetCurrentSubStateIndex()]->SubStateUpdate(input, dt);
			break;
		}

		// check for sub state transition
		if (current_sub_state_ != play_sub_states_[GetCurrentSubStateIndex()]->next_sub_state_type())
			ChangeSubState();

		return true;
	}

	void StatePlay::NextLevel()
	{
		// Init Level
		level_manager_.InitLevel(mesh_data_manager_, level_manager_.current_level().level_number() + 1);
		level_counter_.UpdateCounter(level_manager_.current_level().level_number() + 1);

		// Init Enemy Manager
		enemy_manager_.Init(mesh_data_manager_, level_manager_.current_level().level_faces(),
			level_manager_.current_level().level_geometry_type(), 8, false);

		// Init Player
		player_.InitPlayer(mesh_data_manager_->scene_meshes()[MeshDataManager::PLAYER_MESH],
			mesh_data_manager_->scene_meshes()[MeshDataManager::SHADOW_IMPOSTER_MESH],
			game_data_, level_manager_.current_level().level_faces(), level_manager_.current_level().bottom_face(),
			level_manager_.current_level().level_geometry_type(), 8, 60, false);
	}

	void StatePlay::StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font)
	{
		// state render

		// Debug: font->RenderText(sprite_renderer, gef::Vector4(10.0f, 500.0f, -0.9f), 1.0f, 0xffffffff, gef::TJ_LEFT, state_text_.c_str());
		
		// Debug: font->RenderText(sprite_renderer, gef::Vector4(780.0f, 400.0f, -0.9f), 1.f, 0xffffffff, gef::TJ_LEFT, "kills: %d", enemy_manager_.enemy_kill_count());

		level_counter_.Render(sprite_renderer, font);

		// render substate UI
		play_sub_states_[GetCurrentSubStateIndex()]->SubStateRender(sprite_renderer, font);
	}

	void StatePlay::StateRender3D(gef::Renderer3D* renderer_3d)
	{
		// Render state stereoscopic 3D objects

		level_manager_.current_level().Render(renderer_3d);
		player_.Render(renderer_3d);
		enemy_manager_.Render(renderer_3d);

		// render substate 3D if required
	}


	void StatePlay::StateCleanUp()
	{
		level_manager_.current_level().CleanUp();
	}

	void StatePlay::AssignSubStates()
	{
		play_sub_states_[SubState::PLAY_ACTIVE - SUB_STATE_PLAY_ENUM_OFFSET] = &active_play_state_;
		play_sub_states_[SubState::PLAY_PAUSE - SUB_STATE_PLAY_ENUM_OFFSET] = &pause_play_state_;
		play_sub_states_[SubState::PLAY_DEAD - SUB_STATE_PLAY_ENUM_OFFSET] = &dead_play_state_;
	}

	void StatePlay::ChangeSubState()
	{
		// cleanup curent sub state
		play_sub_states_[GetCurrentSubStateIndex()]->SubStateCleanUp();

		// check if we are retruning from pause state
		bool returning_from_pause = false;
		if (current_sub_state_ == SubState::PLAY_PAUSE) returning_from_pause = true;

		// set new current substate
		current_sub_state_ = play_sub_states_[GetCurrentSubStateIndex()]->next_sub_state_type();
		
		// Check for state exit
		if (current_sub_state_ == SubState::MENU_MAIN)
		{
			next_state_type_ = MENU;
		}
		// check if resuming from pause
		else if (!returning_from_pause)
		{
			// check for substate-specific initialisation
			switch (current_sub_state_)
			{
			case SubState::PLAY_ACTIVE:
				static_cast<StatePlayActive*>(play_sub_states_[GetCurrentSubStateIndex()])->ActivePlayStateReset(&player_, &enemy_manager_);
				break;
			default:
				play_sub_states_[GetCurrentSubStateIndex()]->SubStateInit(*platform_, camera_);
				break;
			}
		}
		else
		{
			// check for substate-specific resumes from pause
			switch (current_sub_state_)
			{
			case SubState::PLAY_ACTIVE:
				static_cast<StatePlayActive*>(play_sub_states_[GetCurrentSubStateIndex()])->ResumeFromPause();
				break;
			default:
				break;
			}
		}
	}

	int StatePlay::GetCurrentSubStateIndex()
	{
		return current_sub_state_ - SUB_STATE_PLAY_ENUM_OFFSET;
	}

}