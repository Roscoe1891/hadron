#ifndef _STATE_H
#define _STATE_H

#include <classes/graphics/camera.h>
#include <classes/graphics/mesh_data_manager.h>
#include <classes/utilities/game_data.h>
#include <input/input_manager.h>
#include <input/keyboard.h>
#include <graphics/font.h>
#include <graphics/sprite_renderer.h>
#include <graphics/renderer_3d.h>
#include <graphics/mesh_instance.h>
#include <platform/d3d11/system/platform_d3d11.h>
#include <string>

// Base Class for game states

namespace s3d_app
{
	// Forward declarations:
	class S3DRenderer;

	class State
	{
	public:
		// Enumerate state types 
		enum StateType { START, MENU, PLAY, GAME_OVER, QUIT, NO_STATES };

		State();
		virtual ~State();

		// Initialise state
		virtual void StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager) = 0;
		// Overloaded Initialise state with s3d renderer
		virtual void StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager) = 0;
		// Update State
		virtual bool StateUpdate(gef::InputManager* input, float dt) = 0;
		// Render stereoscopic 3d state elements
		virtual void StateRender3D(gef::Renderer3D* renderer_3d) = 0;
		// Render UI state elements
		virtual void StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font) = 0;
		// Free memory used by state
		virtual void StateCleanUp() = 0;

		// get this state's type
		inline const StateType state_type() { return state_type_; }
		// get the next state's type
		inline const StateType next_state_type() { return next_state_type_; }

	protected:
		// This state's type
		StateType state_type_;
		// Next state's type
		StateType next_state_type_;
		// The name of this state
		std::string state_text_;

		// Pointer to game data
		GameData* game_data_;
	};
}

#endif // !_STATE_H

