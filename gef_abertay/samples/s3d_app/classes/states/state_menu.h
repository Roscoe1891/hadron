#ifndef _STATE_MENU_H
#define _STATE_MENU_H

#include "state.h"
#include <classes/states/sub_states/state_menu_main.h>
#include <classes/states/sub_states/state_menu_settings.h>
#include <classes/states/sub_states/state_menu_help.h>
#include <classes/states/sub_states/state_menu_credits.h>
#include <classes/gui/text_menu.h>
#include <classes/game_object/projectile.h>
#include <classes/utilities/rng.h>

namespace s3d_app
{
	class StateMenu : public State
	{
// The max number of projectiles used in the display
#define MAX_PROJECTILES 100

	public:
		StateMenu();
		~StateMenu();

		// From base state
		virtual void StateInit(gef::Platform& platform, S3DRenderer* s3d_renderer, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual void StateInit(gef::Platform& platform, Camera* camera, GameData* game_data, MeshDataManager* mesh_data_manager);
		virtual bool StateUpdate(gef::InputManager* input, float dt);
		virtual void StateRender3D(gef::Renderer3D* renderer_3d);
		virtual void StateRenderUI(gef::SpriteRenderer* sprite_renderer, gef::Font* font);
		virtual void StateCleanUp();

	private:
		// Assign sub states to the sub state array
		void AssignSubStates();
		// Change to the next sub state
		void ChangeSubState();
		// Spawn a display projectile
		void SpawnProjectile();

		// Return a random position as Vector2
		gef::Vector2 GetRandomPosition_XY();
		// Return a random position as Vector4
		gef::Vector4 GetRandomPosition_XYZ();

		// the current sub state
		SubState::SubStateType current_sub_state_;
		// Array of pointers to the sub states
		SubState* menu_sub_states_[SubState::NO_MENU_SUB_STATES];

		// The main menu sub state
		StateMenuMain main_menu_state_;
		// The settings menu sub state
		StateMenuSettings settings_menu_state_;
		// The help sub state
		StateMenuHelp help_menu_state_;
		// The credits menu state
		StateMenuCredits credits_menu_state_;

		// pointer to the platform
		gef::Platform* platform_; 
		// pointer to the camera
		Camera* camera_;

		// random number generator
		RNG rng_;
		// pointer to the s3d renderer
		S3DRenderer* s3d_renderer_;

		// The projectiles used for the display
		Projectile projectiles_[MAX_PROJECTILES];
		float projectile_spawn_timer_;
		float k_projectile_spawn_delay_;
		const gef::Vector4 k_projectile_origin_;
		const gef::Vector4 k_projectile_velocity_;
		const int k_projectile_XY_max_;
		const int k_projectile_XY_min_;

		// UI elements
		Text header_text_;
	};
}

#endif

