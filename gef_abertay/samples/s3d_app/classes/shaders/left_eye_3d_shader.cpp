
#include "left_eye_3d_shader.h"
#include <graphics/shader_interface.h>
#include <graphics/mesh_instance.h>
#include <graphics/primitive.h>
#include <system/debug_log.h>
#include <graphics/mesh.h>
#include <graphics/material.h>
#include <graphics/colour.h>
#include <graphics/default_3d_shader_data.h>

namespace s3d_app
{
	LeftEye3DShader::LeftEye3DShader(const gef::Platform& platform)
	{
		device_interface_ = gef::ShaderInterface::Create(platform);

		bool success = true;

		// load vertex shader source in from a file
		char* vs_shader_source = NULL;
		Int32 vs_shader_source_length = 0;
		success = LoadShader("default_3d_shader_vs", "shaders/gef", &vs_shader_source, vs_shader_source_length, platform);

		char* ps_shader_source = NULL;
		Int32 ps_shader_source_length = 0;
		success = LoadShader("left_eye_3d_shader_ps", "shaders/s3d", &ps_shader_source, ps_shader_source_length, platform);

		device_interface_->SetVertexShaderSource(vs_shader_source, vs_shader_source_length);
		device_interface_->SetPixelShaderSource(ps_shader_source, ps_shader_source_length);

		delete[] vs_shader_source;
		vs_shader_source = NULL;
		delete[] ps_shader_source;
		ps_shader_source = NULL;

		wvp_matrix_variable_index_ = device_interface_->AddVertexShaderVariable("wvp", gef::ShaderInterface::kMatrix44);
		world_matrix_variable_index_ = device_interface_->AddVertexShaderVariable("world", gef::ShaderInterface::kMatrix44);
		invworld_matrix_variable_index_ = device_interface_->AddVertexShaderVariable("invworld", gef::ShaderInterface::kMatrix44);
		light_position_variable_index_ = device_interface_->AddVertexShaderVariable("light_position", gef::ShaderInterface::kVector4, 4);

		// pixel shader variables
		// TODO - probable need to keep these separate for D3D11
		material_colour_variable_index_ = device_interface_->AddPixelShaderVariable("material_colour", gef::ShaderInterface::kVector4);
		ambient_light_colour_variable_index_ = device_interface_->AddPixelShaderVariable("ambient_light_colour", gef::ShaderInterface::kVector4);
		light_colour_variable_index_ = device_interface_->AddPixelShaderVariable("light_colour", gef::ShaderInterface::kVector4, 4);

		texture_sampler_index_ = device_interface_->AddTextureSampler("texture_sampler");

		device_interface_->AddVertexParameter("position", gef::ShaderInterface::kVector3, 0, "POSITION", 0);
		device_interface_->AddVertexParameter("normal", gef::ShaderInterface::kVector3, 12, "NORMAL", 0);
		device_interface_->AddVertexParameter("uv", gef::ShaderInterface::kVector2, 24, "TEXCOORD", 0);
		device_interface_->set_vertex_size(sizeof(gef::Mesh::Vertex));
		device_interface_->CreateVertexFormat();

		success = device_interface_->CreateProgram();
	}

	LeftEye3DShader::~LeftEye3DShader()
	{
	}
}
