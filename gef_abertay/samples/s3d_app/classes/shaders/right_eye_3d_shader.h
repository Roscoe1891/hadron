

#ifndef RIGHT_EYE_3D_SHADER_H_
#define RIGHT_EYE_3D_SHADER_H_

#include <graphics/default_3d_shader.h>
#include <gef.h>
#include <maths/vector4.h>
#include <maths/Matrix44.h>

namespace s3d_app
{
	class RightEye3DShader : public gef::Default3DShader
	{
	public:
		RightEye3DShader(const gef::Platform& platform);
		virtual ~RightEye3DShader();
	};

}

#endif /* RIGHT_EYE_3D_SHADER_H_ */
