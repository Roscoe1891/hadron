/*
 * default_sprite_shader.cpp
 *
 *  Created on: 28 Jan 2015
 *      Author: grant
 */

#include "s3d_shader.h"
#include <graphics/shader_interface.h>
#include <system/file.h>
#include <system/platform.h>
#include <system/debug_log.h>
#include <string>
#include <graphics/sprite.h>
#include <math.h>

namespace s3d_app
{
	S3DShader::S3DShader(const gef::Platform& platform)
	{
		device_interface_ = gef::ShaderInterface::Create(platform);

		bool success = true;

		// load vertex shader source in from a file
		char* vs_shader_source = NULL;
		Int32 vs_shader_source_length = 0;
		success = LoadShader("default_sprite_shader_vs", "shaders/gef", &vs_shader_source, vs_shader_source_length, platform);

		char* ps_shader_source = NULL;
		Int32 ps_shader_source_length = 0;
		success = LoadShader("s3d_ps", "shaders/s3d", &ps_shader_source, ps_shader_source_length, platform);

		device_interface_->SetVertexShaderSource(vs_shader_source, vs_shader_source_length);
		device_interface_->SetPixelShaderSource(ps_shader_source, ps_shader_source_length);

		delete[] vs_shader_source;
		vs_shader_source = NULL;
		delete[] ps_shader_source;
		ps_shader_source = NULL;


		projection_matrix_variable_index_ = device_interface_->AddVertexShaderVariable("proj_matrix", gef::ShaderInterface::kMatrix44);
		sprite_data_variable_index_ = device_interface_->AddVertexShaderVariable("sprite_data", gef::ShaderInterface::kMatrix44);
		left_eye_texture_sampler_index_ = device_interface_->AddTextureSampler("left_eye_texture");
		right_eye_texture_sampler_index_ = device_interface_->AddTextureSampler("right_eye_texture");

		device_interface_->AddVertexParameter("position", gef::ShaderInterface::kVector3, 0, "POSITION", 0);
		device_interface_->set_vertex_size(12);

		device_interface_->CreateVertexFormat();

		success = device_interface_->CreateProgram();
	}

	S3DShader::~S3DShader()
	{

	}


	void S3DShader::SetSpriteData(const gef::Sprite& sprite, const gef::Texture* left_eye_texture, const gef::Texture* right_eye_texture)
	{
		gef::Matrix44 sprite_data;
		BuildSpriteShaderData(sprite, sprite_data);

		device_interface_->SetVertexShaderVariable(sprite_data_variable_index_, &sprite_data);
		device_interface_->SetTextureSampler(left_eye_texture_sampler_index_, left_eye_texture);
		device_interface_->SetTextureSampler(right_eye_texture_sampler_index_, right_eye_texture);
	}

}