#ifndef S3D_SHADER_H_
#define S3D_SHADER_H_

#include <graphics/default_sprite_shader.h>
#include <gef.h>

namespace gef
{
	class Sprite;
	class Matrix44;
	class Texture;
}

namespace s3d_app
{
	class S3DShader : public gef::DefaultSpriteShader
	{
	public:
		S3DShader(const gef::Platform& platform);
		~S3DShader();

		void SetSpriteData(const gef::Sprite& sprite, const gef::Texture* left_eye_texture, const gef::Texture* right_eye_texture);

	protected:
		Int32 left_eye_texture_sampler_index_;
		Int32 right_eye_texture_sampler_index_;
	};

}

#endif /* S3D_SHADER_H_ */
