

#ifndef LEFT_EYE_3D_SHADER_H_
#define LEFT_EYE_3D_SHADER_H_

#include <graphics/default_3d_shader.h>
#include <graphics/shader.h>
#include <gef.h>
#include <maths/vector4.h>
#include <maths/Matrix44.h>

#define MAX_NUM_POINT_LIGHTS 4

namespace gef
{
	class MeshInstance;
	class Primitive;
	class Texture;
	class Material;
	class Default3DShaderData;
}

namespace s3d_app
{
	class LeftEye3DShader : public gef::Default3DShader
	{
	public:
		LeftEye3DShader(const gef::Platform& platform);
		virtual ~LeftEye3DShader();
	};
}

#endif /* LEFT_EYE_3D_SHADER_H_ */
