#ifndef _CAMERA_H
#define _CAMERA_H

#include <maths/vector4.h>
#include <maths/math_utils.h>

/// Camera provides our view of the game world, and serves as the eye position in S3D calculations

namespace s3d_app
{
	class Camera
	{
	public:
		Camera();
		~Camera();

		// Initialise camera
		void Init();
		// Get Forward Vector
		gef::Vector4 GetForward();
		// Get Right Vector
		gef::Vector4 GetRight();

		// Get camera's position
		inline gef::Vector4 position() const { return position_; }
		// Get camera's look at
		inline gef::Vector4 look_at() const { return look_at_; }
		// Get up vector
		inline gef::Vector4 up() const { return up_; }
		// Get camera's field of view
		inline float fov() const { return fov_; }
		// Get near plane
		inline float near_plane() const { return near_plane_; }
		// Get far plane
		inline float far_plane() const { return far_plane_; }
		// Set far plane to 'new value'
		inline void set_far_plane(float new_value) { far_plane_ = new_value; }

	private:
		// Camera's position
		gef::Vector4 position_;
		// Camera's look at vector
		gef::Vector4 look_at_;
		// Camera's up vector
		gef::Vector4 up_;
		// Field of view
		float fov_;
		// Near plane proximity
		float near_plane_;
		// Far plane proximity
		float far_plane_;
	};
}


#endif // !_CAMERA_H