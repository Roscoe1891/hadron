#include "camera.h"

namespace s3d_app
{
	Camera::Camera()
	{
	}

	Camera::~Camera()
	{
	}

	// initialise the camera settings
	void Camera::Init()
	{
		// assign values
		position_ = gef::Vector4(0.f, 0.f, 85.0f);
		look_at_ = gef::Vector4(0.0f, 0.0f, 0.0f);
		up_ = gef::Vector4(0.0f, 1.0f, 0.0f);
		fov_ = gef::DegToRad(45.0f);
		near_plane_ = 0.01f;
		far_plane_ = 190.f;
	}

	gef::Vector4 Camera::GetForward()
	{
		// calculate and return forward vector
		gef::Vector4 result = look_at_ - position_;
		return result;
	}

	gef::Vector4 Camera::GetRight()
	{		
		// calculate and return right vector
		gef::Vector4 result = GetForward();
		result.Normalise();
		result = result.CrossProduct(up_);
		return result;
	}

}
