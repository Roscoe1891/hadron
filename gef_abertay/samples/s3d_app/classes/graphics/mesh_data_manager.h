#ifndef _MESH_DATA_MANAGER_H
#define _MESH_DATA_MANAGER_H

#include <assets/png_loader.h>

#include <graphics/scene.h>
#include <graphics/material.h>
#include <graphics/texture.h>
#include <graphics/image_data.h>

#include <classes/game_object/game_object.h>
#include <classes/graphics/mesh_builder.h>
#include <system/platform.h>


namespace s3d_app
{
	class MeshDataManager
	{
	public:
		// Define Mesh Type
		enum MeshTypes { PLAYER_MESH, ENEMY_MESH, PROJECTILE_MESH, LEVEL1_MESH, LEVEL2_MESH, SHADOW_IMPOSTER_MESH, MAX_MESHES };
		// Define Material Types
		enum MaterialTypes { PLAYER_MAT, ENEMY_MAT, PROJECTILE_MAT, LEVEL1_MAT, LEVEL2_MAT, SHADOW_IMPOSTER_MAT, MAX_MATS };
		// Define an array of mesh pointers
		typedef gef::Mesh* MeshArray[MAX_MESHES];

		MeshDataManager();
		~MeshDataManager();

		// Build all meshes which will be required
		bool BuildMeshes(gef::Platform& platform);
		// Release memory (called from state upon exiting the app)
		void CleanUp();

		// return pointer to first model in model array
		inline gef::Scene* models_scene() const { return models_scene_; }
		// reference to scene mesh MeshArray
		inline MeshArray& scene_meshes() { return scene_meshes_; }
		// return pointer to first material in material array
		inline gef::Material* scene_materials() { return scene_materials_; }

	private:
		// Create the materials our models require
		void CreateMaterials(gef::Platform& platform);		
		// Load models from external files
		bool LoadModels(gef::Platform& platform);	
		// Create meshes from the models
		void CreateMeshes(gef::Platform& platform);			

		// used to construct any meshes which are not loaded in but generated in code
		MeshBuilder mesh_builder_;							

		// scene data loaded from .scn file
		gef::Scene* models_scene_;	
		// holds all meshes
		MeshArray scene_meshes_;			
		// holds all textures for the materials
		gef::Texture* textures_[MAX_MATS];		
		// holds all materials for the meshes
		gef::Material scene_materials_[MAX_MATS];			


	};
}

#endif