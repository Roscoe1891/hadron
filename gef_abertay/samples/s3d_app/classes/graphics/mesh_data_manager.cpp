#include "mesh_data_manager.h"

namespace s3d_app
{
	MeshDataManager::MeshDataManager()
	{
	}

	MeshDataManager::~MeshDataManager()
	{
	}

	void MeshDataManager::CleanUp()
	{
		// release scene
		delete models_scene_;
		models_scene_ = nullptr;

		// release all meshes
		for (int i = 0; i < MAX_MESHES; ++i)
		{
			if (scene_meshes_[i])
			{
				delete scene_meshes_[i];
				scene_meshes_[i] = nullptr;
			}
		}

		// release all textures
		for (int i = 0; i < MAX_MATS; ++i)
		{
			if (textures_[i])
			{
				delete textures_[i];
				textures_[i] = nullptr;
			}
		}
	}

	bool MeshDataManager::BuildMeshes(gef::Platform& platform)
	{
		// Build all meshes which will be required

		// Create the materials required for the meshes we're about to create
		CreateMaterials(platform);

		// Load models from external files
		if (!LoadModels(platform))
			return false;

		// Create meshes using mesh builder
		CreateMeshes(platform);
		
	}

	bool MeshDataManager::LoadModels(gef::Platform& platform)
	{
		// Load models from external files

		bool result = true;

		// create a new scene object and read in the data from the file
		models_scene_ = new gef::Scene();
		models_scene_->ReadSceneFromFile(platform, "sphere.scn");

		// required if we want to render the data stored in the scene file so lets create the materials from the material data present in the scene file
		// NOT USED IN THIS CASE - MATERIALS HELD SEPERATELY
		// model_scene_->CreateMaterials(platform_);

		// check to see if we have mesh data in the file and create the meshes required
		if (models_scene_->meshes.size()> 0)
		{
			scene_meshes_[PLAYER_MESH] = models_scene_->CreateMesh(platform, models_scene_->meshes.front(), &scene_materials_[PLAYER_MAT]);
			scene_meshes_[ENEMY_MESH] = models_scene_->CreateMesh(platform, models_scene_->meshes.front(), &scene_materials_[ENEMY_MAT]);
			scene_meshes_[PROJECTILE_MESH] = models_scene_->CreateMesh(platform, models_scene_->meshes.front(), &scene_materials_[PROJECTILE_MAT]);
		}
		else
			result = false;

		// dump the old scene object and create a new one
		delete models_scene_;
		models_scene_ = new gef::Scene();

		// read next file
		models_scene_->ReadSceneFromFile(platform, "cylinder_8.scn");
		// check to see if we have mesh data in the file and create the meshes required
		if (models_scene_->meshes.size()> 0)
		{
			scene_meshes_[LEVEL1_MESH] = models_scene_->CreateMesh(platform, models_scene_->meshes.front(), &scene_materials_[LEVEL1_MAT]);
		}
		else
			result = false;

		// dump the old scene object and create a new one
		delete models_scene_;
		models_scene_ = new gef::Scene();

		// read next file
		models_scene_->ReadSceneFromFile(platform, "cylinder_12.scn");
		// check to see if we have mesh data in the file and create the meshes required
		if (models_scene_->meshes.size()> 0)
		{
			scene_meshes_[LEVEL2_MESH] = models_scene_->CreateMesh(platform, models_scene_->meshes.front(), &scene_materials_[LEVEL2_MAT]);
		}
		else
			result = false;

		return result;
	}

	void MeshDataManager::CreateMaterials(gef::Platform& platform)
	{
		// Create the materials our models require

		// create png loader
		gef::PNGLoader png_loader;
		gef::ImageData image_data[MAX_MATS];

		// Load image data from files
		png_loader.Load("white.png", platform, image_data[PLAYER_MAT]);
		png_loader.Load("grey_4.png", platform, image_data[ENEMY_MAT]);
		png_loader.Load("grey_4.png", platform, image_data[PROJECTILE_MAT]);
		png_loader.Load("grey_striped_8.png", platform, image_data[LEVEL1_MAT]);
		png_loader.Load("grey_striped_12.png", platform, image_data[LEVEL2_MAT]);
		png_loader.Load("shadow_imposter.png", platform, image_data[SHADOW_IMPOSTER_MAT]);

		// Create textures and assign to materials
		textures_[PLAYER_MAT] = textures_[PLAYER_MAT]->Create(platform, image_data[PLAYER_MAT]);
		scene_materials_[PLAYER_MAT].set_texture(textures_[PLAYER_MAT]);

		textures_[ENEMY_MAT] = textures_[ENEMY_MAT]->Create(platform, image_data[ENEMY_MAT]);
		scene_materials_[ENEMY_MAT].set_texture(textures_[ENEMY_MAT]);

		textures_[PROJECTILE_MAT] = textures_[PROJECTILE_MAT]->Create(platform, image_data[PROJECTILE_MAT]);
		scene_materials_[PROJECTILE_MAT].set_texture(textures_[PROJECTILE_MAT]);

		textures_[LEVEL1_MAT] = textures_[LEVEL1_MAT]->Create(platform, image_data[LEVEL1_MAT]);
		scene_materials_[LEVEL1_MAT].set_texture(textures_[LEVEL1_MAT]);

		textures_[LEVEL2_MAT] = textures_[LEVEL2_MAT]->Create(platform, image_data[LEVEL2_MAT]);
		scene_materials_[LEVEL2_MAT].set_texture(textures_[LEVEL2_MAT]);

		textures_[SHADOW_IMPOSTER_MAT] = textures_[SHADOW_IMPOSTER_MAT]->Create(platform, image_data[SHADOW_IMPOSTER_MAT]);
		scene_materials_[SHADOW_IMPOSTER_MAT].set_texture(textures_[SHADOW_IMPOSTER_MAT]);

	}

	void MeshDataManager::CreateMeshes(gef::Platform& platform)
	{
		// Build meshes using mesh builder (rather than loading from file)
		scene_meshes_[SHADOW_IMPOSTER_MESH] = mesh_builder_.CreateQuadMesh(platform, 4.f, &scene_materials_[SHADOW_IMPOSTER_MAT]);
	}


}