#ifndef _MESH_BUILDER_H
#define _MESH_BUILDER_H

#include <graphics/material.h>
#include <graphics/mesh.h>
#include <graphics/primitive.h>
#include <system/platform.h>

/// Mesh builder is used to create meshes generated in code

namespace s3d_app
{
	class MeshBuilder
	{
	public:
		MeshBuilder();
		~MeshBuilder();

		// Return a new cube mesh
		gef::Mesh* CreateCubeMesh(gef::Platform& platform, gef::Material* material = nullptr);
		// Return a new quad mesh
		gef::Mesh* CreateQuadMesh(gef::Platform& platform, float size, gef::Material* material = nullptr);

	private:

	};
}

#endif // ! _MESH_BUILDER_H
