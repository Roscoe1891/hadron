#ifndef _SHADOW_IMPOSTER_H
#define _SHADOW_IMPOSTER_H

#include <graphics/mesh_instance.h>
#include <graphics/renderer_3d.h>

/// A shadow imposter is a type of mesh instance attached to an actor
/// to give the impression of a shadow in the game world

namespace s3d_app
{
	class ShadowImposter : public gef::MeshInstance
	{
	public:
		ShadowImposter();
		~ShadowImposter();

		// Initialise shadow imposter
		void Init(gef::Mesh* mesh, gef::Vector4 position, gef::Matrix44& parent_transform);
		// Update shadow imposter with parent transform matrix 
		void Update(gef::Matrix44& parent_transform);
		// Render shadow imposter
		void Render(gef::Renderer3D* renderer_3d);

	private:
		// shadow's local position (relative to actor to which it's attached)
		gef::Vector4 position_;		
		// update this shadow's transform relative to actor (parent) transform
		void UpdateTransform(gef::Matrix44& parent_transform);
		// shadow's local transform (relative to actor to which it's attached)
		gef::Matrix44 local_transform_;
	};
}
#endif