#include "s3d_renderer.h"

namespace s3d_app
{
	S3DRenderer::S3DRenderer() :
		renderer_3d_(NULL),
		s3d_shader_(NULL), 
		left_eye_shader_(NULL), 
		right_eye_shader_(NULL)
	{
	}

	S3DRenderer::~S3DRenderer()
	{
	}

	void S3DRenderer::Init(gef::Platform& platform)
	{
		// Create 3D renderer
		renderer_3d_ = platform.CreateRenderer3D();

		//renderer_3d_->SetFillMode(gef::Renderer3D::kWireframe);

		// Create Render Targets
		CreateRenderTargets(platform);

		// Set up 3D sprite for drawing our scene
		SetupS3DSprite(platform);

		// Set up debug sprites (for drawing each eye view seperately)
		if (SHOW_DEBUG_SPRITES) SetupDebugSprites(platform);

		// Create S3D, left and right eye shaders
		CreateS3DShaders(platform);

		// Set S3D values
		eye_separation_distance_ = 0.032f;

		// convergence distance (default 1.74)
		convergence_distance_ = 2.32f;

		// clear the screen to black (required for S3D to work!)
		platform.set_render_target_clear_colour(gef::Colour(0.0f, 0.0f, 0.0f, 1.f));
	}

	void S3DRenderer::CreateRenderTargets(gef::Platform& platform)
	{
		// create left and right eye render targets
		left_eye_render_target_ = platform.CreateRenderTarget();
		right_eye_render_target_ = platform.CreateRenderTarget();
	}

	void S3DRenderer::SetupS3DSprite(gef::Platform& platform)
	{
		// set position, width and height of S3D Sprite
		s3d_sprite_.set_position(gef::Vector4(platform.width()*0.5f, platform.height()*0.5f, 0.5f));
		s3d_sprite_.set_width(platform.width());
		s3d_sprite_.set_height(platform.height());
	}

	void S3DRenderer::SetupDebugSprites(gef::Platform& platform)
	{
		// Set width, position height and texture for debug sprite left
		left_eye_debug_sprite_.set_position(gef::Vector4(platform.width()*0.25f, platform.height()*0.25f, 0.0f));
		left_eye_debug_sprite_.set_width(platform.width()*0.5f);
		left_eye_debug_sprite_.set_height(platform.height()*0.5f);
		left_eye_debug_sprite_.set_texture(left_eye_render_target_->texture());

		// Set width, position height and texture for debug sprite right
		right_eye_debug_sprite_.set_position(gef::Vector4(platform.width()*0.75f, platform.height()*0.25f, 0.0f));
		right_eye_debug_sprite_.set_width(platform.width()*0.5f);
		right_eye_debug_sprite_.set_height(platform.height()*0.5f);
		right_eye_debug_sprite_.set_texture(right_eye_render_target_->texture());
	}

	
	void S3DRenderer::CreateS3DShaders(gef::Platform& platform)
	{
		// Create the S3d, left and right eye shaders
		gef::PlatformD3D11& platform_d3d11 = static_cast<gef::PlatformD3D11&>(platform);
		left_eye_shader_ = new s3d_app::LeftEye3DShader(platform);
		right_eye_shader_ = new s3d_app::RightEye3DShader(platform);
		s3d_shader_ = new s3d_app::S3DShader(platform);
	}

	// Render a 3D scene to the S3D sprite
	void S3DRenderer::Render3DScene(gef::Platform& platform, gef::SpriteRenderer* sprite_renderer, Camera* camera, State* state_scene_to_render)
	{
		// Calc & set view matrices
		gef::Matrix44 view_matrix, left_eye_view_matrix, right_eye_view_matrix;
		SetViewMatrices(camera, &view_matrix, &left_eye_view_matrix, &right_eye_view_matrix);

		// Calc & set projection matrices
		gef::Matrix44 projection_matrix, left_eye_projection_matrix, right_eye_projection_matrix;
		SetProjectionMatrices(platform, camera, &projection_matrix, &left_eye_projection_matrix, &right_eye_projection_matrix);

		// Render scene for left eye
		left_eye_shader_->SetSceneData(renderer_3d_->default_shader_data(), left_eye_view_matrix, left_eye_projection_matrix);
		RenderScene(platform, left_eye_render_target_, left_eye_shader_, left_eye_view_matrix, left_eye_projection_matrix, state_scene_to_render);

		// Render scene for right eye
		right_eye_shader_->SetSceneData(renderer_3d_->default_shader_data(), right_eye_view_matrix, right_eye_projection_matrix);
		RenderScene(platform, right_eye_render_target_, right_eye_shader_, right_eye_view_matrix, right_eye_projection_matrix, state_scene_to_render);

		// set the render target back to the default
		platform.set_render_target(NULL);

		// clear the render target before drawing anything by passing true into Begin
		sprite_renderer->Begin(true);

		// debug: draw contents of left and right render targets
		if (SHOW_DEBUG_SPRITES) DrawDebugSprites(sprite_renderer);
		// finish drawing debug sprites
		sprite_renderer->End();

		// FINAL RENDER PASS:
		// Start drawing, do not clear render target
		sprite_renderer->Begin(false);

		// enable required shader resources
		SetS3DShaderResources(platform, sprite_renderer);

		// Draw the sprite with our S3D image
		sprite_renderer->DrawSprite(s3d_sprite_);

		// unbind S3D resources
		s3d_shader_->device_interface()->UnbindTextureResources(platform);

		// finished drawing
		sprite_renderer->End();

		// set the sprite shader back to the default sprite shader
		sprite_renderer->SetShader(NULL);
	}

	void S3DRenderer::SetViewMatrices(Camera* camera, gef::Matrix44* view_matrix, gef::Matrix44* left_eye_view_matrix, gef::Matrix44* right_eye_view_matrix)
	{
		// set view matrix look at
		view_matrix->LookAt(camera->position(), camera->look_at(), camera->up());

		// calculate eye seperation vector
		gef::Vector4 eye_seperation_vector = camera->GetRight() * (eye_separation_distance_ * 0.5f);

		// calculate left eye camera position & look at
		gef::Vector4 left_eye_cam_pos = camera->position() - eye_seperation_vector;
		gef::Vector4 left_eye_lookat = camera->look_at() - eye_seperation_vector;

		// calculate right eye camera position & look at
		gef::Vector4 right_eye_cam_pos = camera->position() + eye_seperation_vector;
		gef::Vector4 right_eye_lookat = camera->look_at() + eye_seperation_vector;

		// set lookat for view matrices for each eye
		left_eye_view_matrix->LookAt(left_eye_cam_pos, left_eye_lookat, gef::Vector4(0.0f, 1.0f, 0.0f));
		right_eye_view_matrix->LookAt(right_eye_cam_pos, right_eye_lookat, gef::Vector4(0.0f, 1.0f, 0.0f));
	}

	void S3DRenderer::SetProjectionMatrices(gef::Platform& platform, Camera* camera, gef::Matrix44* projection_matrix, gef::Matrix44* left_eye_projection_matrix, gef::Matrix44* right_eye_projection_matrix)
	{
		// Set projection matrix
		*projection_matrix = platform.PerspectiveProjectionFov(camera->fov(), (float)platform.width() / (float)platform.height(), camera->near_plane(), camera->far_plane());

		// calculate the height of the view plane at the near distance.
		float height = tanf(camera->fov() * 0.5f) * camera->near_plane();
		float aspect_ratio = (float)platform.width() / (float)platform.height();

		// calculate projection parameters
		float a = tanf(camera->fov() * 0.5) * aspect_ratio * convergence_distance_;
		float b = a - (eye_separation_distance_ * 0.5f);
		float c = a + (eye_separation_distance_ * 0.5f);
		float d_left = camera->near_plane() * (b / convergence_distance_);
		float d_right = camera->near_plane() * (c / convergence_distance_);

		// set perspective matrices for each eye
		*left_eye_projection_matrix = platform.PerspectiveProjectionFrustum(-d_left, d_right, height, -height, camera->near_plane(), camera->far_plane());
		*right_eye_projection_matrix = platform.PerspectiveProjectionFrustum(-d_right, d_left, height, -height, camera->near_plane(), camera->far_plane());
	}

	void S3DRenderer::SetS3DShaderResources(gef::Platform& platform, gef::SpriteRenderer* sprite_renderer)
	{
		// Set Shader Resources for s3d shader
		sprite_renderer->SetShader(s3d_shader_);
		s3d_shader_->device_interface()->UseProgram();
		s3d_shader_->SetSceneData(sprite_renderer->projection_matrix());
		s3d_shader_->device_interface()->SetVertexFormat();
		s3d_shader_->SetSpriteData(s3d_sprite_, left_eye_render_target_->texture(), right_eye_render_target_->texture());
		s3d_shader_->device_interface()->SetVariableData();
		s3d_shader_->device_interface()->BindTextureResources(platform);
	}

	void S3DRenderer::DrawDebugSprites(gef::SpriteRenderer* sprite_renderer)
	{
		// draw sprites
		sprite_renderer->DrawSprite(left_eye_debug_sprite_);
		sprite_renderer->DrawSprite(right_eye_debug_sprite_);
	}

	void S3DRenderer::RenderScene(gef::Platform& platform, gef::RenderTarget* render_target, gef::Shader* shader, const gef::Matrix44& view_matrix, const gef::Matrix44& projection_matrix, State* state_scene)
	{
		platform.set_render_target(render_target);
		renderer_3d_->set_projection_matrix(projection_matrix);
		renderer_3d_->set_view_matrix(view_matrix);
		renderer_3d_->SetShader(shader);

		// clear the render target before drawing anything by passing true into Begin
		renderer_3d_->Begin(true);
		// render current state scene
		state_scene->StateRender3D(renderer_3d_);
		renderer_3d_->End();
	}

	

	void S3DRenderer::CleanUp()
	{
		delete renderer_3d_;
		renderer_3d_ = NULL;

		delete left_eye_render_target_;
		left_eye_render_target_ = NULL;

		delete right_eye_render_target_;
		right_eye_render_target_ = NULL;

		delete s3d_shader_;
		s3d_shader_ = NULL;

		delete left_eye_shader_;
		left_eye_shader_ = NULL;

		delete right_eye_shader_;
		right_eye_shader_ = NULL;
	}
}