#include "mesh_builder.h"

namespace s3d_app
{
	MeshBuilder::MeshBuilder()
	{
	}

	MeshBuilder::~MeshBuilder()
	{
	}

	gef::Mesh* MeshBuilder::CreateQuadMesh(gef::Platform& platform, float size, gef::Material* material)
	{
		gef::Mesh* mesh = platform.CreateMesh();

		// initialise the vertex data to create a quad of specified size
		const float half_size = size * 0.5f;
		const gef::Mesh::Vertex vertices[] = {
																						// (Looking down y axis)
		{ -half_size,  0.f,  half_size, -0.577f,  0.577f,  0.577f, 0.0f, 1.0f },		//  Bottom left
		{ -half_size,  0.f, -half_size, -0.577f,  0.577f, -0.577f, 0.0f, 0.0f },		//	Top left
		{ half_size,  0.f, -half_size,  0.577f,  0.577f, -0.577f, 1.0f, 0.0f },			//	Top right
		{ half_size,  0.f,  half_size,  0.577f,  0.577f,  0.577f, 1.0f, 1.0f },			//	Bottom left

		};

		// Initialise the vertex buffer
		mesh->InitVertexBuffer(platform, static_cast<const void*>(vertices), sizeof(vertices) / sizeof(gef::Mesh::Vertex), sizeof(gef::Mesh::Vertex));

		// create a single triangle list primitive to draw the triangles that make up the quad
		mesh->AllocatePrimitives(1);
		gef::Primitive* primitive = mesh->GetPrimitive(0);

		// define indices
		const UInt32 indices[] = {
			0, 1, 2,
			2, 3, 0,
		};

		// Initialise index buffer
		primitive->InitIndexBuffer(platform, static_cast<const void*>(indices), sizeof(indices) / sizeof(UInt32), sizeof(UInt32));
		primitive->set_type(gef::TRIANGLE_LIST);

		// If a material has been specified, assign it
		if (material)
			primitive->set_material(material);

		return mesh;
	}

	gef::Mesh* MeshBuilder::CreateCubeMesh(gef::Platform& platform, gef::Material* material)
	{
		gef::Mesh* mesh = platform.CreateMesh();
	
		// initialise the vertex data to create a 1, 1, 1 cube
		const float half_size = 0.5f;
		gef::Mesh::Vertex vertices[] = {
			{ half_size, -half_size, -half_size,  0.577f, -0.577f, -0.577f, 0.0f, 0.0f },		// 0
			{ half_size,  half_size, -half_size,  0.577f,  0.577f, -0.577f, 0.0f, 0.0f },		// 1
			{ -half_size,  half_size, -half_size, -0.577f,  0.577f, -0.577f, 0.0f, 0.0f },		// 2
			{ -half_size, -half_size, -half_size, -0.577f, -0.577f, -0.577f, 0.0f, 0.0f },		// 3
			{ half_size, -half_size,  half_size,  0.577f, -0.577f,  0.577f, 0.0f, 0.0f },		// 4
			{ half_size,  half_size,  half_size,  0.577f,  0.577f,  0.577f, 0.0f, 0.0f },		// 5
			{ -half_size,  half_size,  half_size, -0.577f,  0.577f,  0.577f, 0.0f, 0.0f },		// 6
			{ -half_size, -half_size,  half_size, -0.577f, -0.577f,  0.577f, 0.0f, 0.0f }		// 7
		};

		// Initialise the vertex buffer
		mesh->InitVertexBuffer(platform, static_cast<const void*>(vertices), sizeof(vertices) / sizeof(gef::Mesh::Vertex), sizeof(gef::Mesh::Vertex));

		// we will create a single triangle list primitive to draw the triangles that make up the cube
		mesh->AllocatePrimitives(1);
		gef::Primitive* primitive = mesh->GetPrimitive(0);

		// define indices
		const UInt32 indices[] = {
			// Back
			0, 1, 2,
			2, 3, 0,
			// Front
			6, 5, 4,
			4, 7, 6,
			// Left
			2, 7, 3,
			2, 6, 7,
			// Right
			0, 4, 1,
			5, 1, 4,
			// Top
			6, 2, 1,
			5, 6, 1,
			// Bottom
			0, 3, 7,
			0, 7, 4
		};

		// Initialise index buffer
		primitive->InitIndexBuffer(platform, static_cast<const void*>(indices), sizeof(indices) / sizeof(UInt32), sizeof(UInt32));
		primitive->set_type(gef::TRIANGLE_LIST);

		// If a material has been specified, assign it
		if (material)
			primitive->set_material(material);

		// set size of bounds, required for collision detection routines
		gef::Aabb aabb(gef::Vector4(-half_size, -half_size, -half_size), gef::Vector4(half_size, half_size, half_size));
		gef::Sphere sphere(aabb);

		// set axis aligned bounding box
		mesh->set_aabb(aabb);
		// set bounding sphere
		mesh->set_bounding_sphere(sphere);

		return mesh;
	}
	
}