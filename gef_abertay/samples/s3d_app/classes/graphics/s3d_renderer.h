#ifndef _S3D_RENDERER_H
#define _S3D_RENDERER_H

#include <graphics/renderer_3d.h>
#include <graphics/render_target.h>
#include <graphics/sprite_renderer.h>
#include <graphics/sprite.h>
#include <graphics/shader_interface.h>
#include <platform/d3d11/system/platform_d3d11.h>

#include <classes/graphics/camera.h>
#include <classes/shaders/left_eye_3d_shader.h>
#include <classes/shaders/right_eye_3d_shader.h>
#include <classes/shaders/s3d_shader.h>
#include <classes/states/state.h>

/// S3D Renderer encapsulates the shaders and renderer required to create the stereoscopic 3D effect
/// The value of SHOW_DEBUG_SPRITES can be set to true to show the individual views created for each eye

#define SHOW_DEBUG_SPRITES false

namespace s3d_app
{
	class S3DRenderer
	{
	public:
		S3DRenderer();
		~S3DRenderer();
		// Initialise S3D Renderer
		void Init(gef::Platform& platform);
		// Create the Render Targets on which to draw the images for eavh eye
		void CreateRenderTargets(gef::Platform& platform);
		// Set up the sprite which will be used to draw the final S3D image
		void SetupS3DSprite(gef::Platform& platform);
		// Set up the sprites which will be used to draw the debug images
		void SetupDebugSprites(gef::Platform& platform);
		// Create the shaders for rendering the scene for the different eyes
		// and the shader that combines both render targets to produce the final image
		void CreateS3DShaders(gef::Platform& platform);
		// Set Viewing matrices
		void SetViewMatrices(Camera* camera, gef::Matrix44* view_matrix, gef::Matrix44* left_eye_view_matrix, gef::Matrix44* right_eye_view_matrix);
		// Set Projection matrices
		void SetProjectionMatrices(gef::Platform& platform, Camera* camera, gef::Matrix44* projection_matrix, gef::Matrix44* left_eye_projection_matrix, gef::Matrix44* right_eye_projection_matrix);
		// Set S3d shader resources
		// Enables required shader resources for final S3D draw pass
		void SetS3DShaderResources(gef::Platform& platform, gef::SpriteRenderer* sprite_renderer);
		// Render the scene to stereoscopic 3D
		void Render3DScene(gef::Platform& platform, gef::SpriteRenderer* sprite_renderer, Camera* camera, State* current_state);
		// Render the scene to render target
		void RenderScene(gef::Platform& platform, gef::RenderTarget* render_target, gef::Shader* shader, const gef::Matrix44& view_matrix, const gef::Matrix44& projection_matrix, State* state_scene);
		// Draw the debug images for each eye
		void DrawDebugSprites(gef::SpriteRenderer* sprite_renderer);
		// Free memory (called by App on exit)
		void CleanUp();

		// return pointer to 3D renderer
		inline gef::Renderer3D* const renderer_3d() { return renderer_3d_; }

		// set the eye seperation distance to 'new value'
		inline void set_eye_seperation_distance(float new_value) { eye_separation_distance_ = new_value; }
		// get the eye seperation distance
		inline float eye_seperation_distance() { return eye_separation_distance_; }

		// set the convergence distance to 'new value'
		inline void set_convergence_distance(float new_value) { convergence_distance_ = new_value; }
		// get the convergence distance
		inline float convergence_distance() { return convergence_distance_; }

	private:
		// 3D renderer (created here)
		class gef::Renderer3D* renderer_3d_;

		// S3D, left and right eye shaders (created here)
		s3d_app::S3DShader* s3d_shader_;
		s3d_app::LeftEye3DShader* left_eye_shader_;
		s3d_app::RightEye3DShader* right_eye_shader_;

		// Left and right eye render targets
		gef::RenderTarget* left_eye_render_target_;
		gef::RenderTarget* right_eye_render_target_;
		// Left and right eye debug sprites
		gef::Sprite left_eye_debug_sprite_;
		gef::Sprite right_eye_debug_sprite_;
		// Sprite for drawing the final stereoscopic 3D image
		gef::Sprite s3d_sprite_;

		// The eye seperation distance
		float eye_separation_distance_;
		// The convergenec distance
		float convergence_distance_;
	};
}

#endif

