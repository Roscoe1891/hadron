#include "shadow_imposter.h"

namespace s3d_app
{
	ShadowImposter::ShadowImposter()
	{
	}

	ShadowImposter::~ShadowImposter()
	{
	}

	void ShadowImposter::Init(gef::Mesh* mesh, gef::Vector4 position, gef::Matrix44& parent_transform)
	{
		// assign mesh from model
		set_mesh(mesh);

		// set position
		position_ = position;

		// reset local transform and set its translation provided position
		local_transform_.SetIdentity();
		local_transform_.SetTranslation(position_);

		// set transform matrix
		UpdateTransform(parent_transform);
	}

	void ShadowImposter::Update(gef::Matrix44& parent_transform)
	{
		UpdateTransform(parent_transform);
	}

	void ShadowImposter::UpdateTransform(gef::Matrix44& parent_transform)
	{
		// multiply local transform by parent transform
		transform_ = local_transform_ * parent_transform;
	}

	void ShadowImposter::Render(gef::Renderer3D* renderer_3d)
	{
		// Draw shadow imposter
		renderer_3d->DrawMesh(*this);
	}

}