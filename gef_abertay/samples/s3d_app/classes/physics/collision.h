#ifndef _COLLISION_H
#define _COLLISION_H

#include <classes/game_object/game_object.h>

namespace s3d_app
{
	class Collision
	{
	public:
		// The type of bounding objects that were used to detect this collision
		enum CollisionType { SPHERE, BOUNDS };

		Collision();
		Collision(GameObject* go_1, GameObject* go_2);
		~Collision();
		
		// get game object 1
		inline GameObject* gameObject_1() const { return gameObject_1_; }

		// get game object 2
		inline GameObject* gameObject_2() const { return gameObject_2_; }

	private:
		// Game objects involved in collision

		GameObject* gameObject_1_;
		GameObject* gameObject_2_;
	};
}

#endif