#include "physics_3d.h"
#include <classes/game_object/game_object.h>

namespace s3d_app
{
	Physics3D::Physics3D()
	{
	}

	Physics3D::~Physics3D()
	{
	}

	void Physics3D::CheckCollision(GameObject* go_1, GameObject* go_2, Collision::CollisionType checkType)
	{
		// check what type of collision is to be used
		switch (checkType)
		{
		// apply appropriate collision check
		case Collision::SPHERE:
			SphereCollision(go_1, go_2);
			break;

		case Collision::BOUNDS:
			BoundsCollision(go_1, go_2);
			break;
		}
	}

	void Physics3D::SphereCollision(GameObject* go_1, GameObject* go_2)
	{
		// Caclulate collision spheres
		gef::Sphere sphere_1(go_1->position(), go_1->mesh()->bounding_sphere().radius() * go_1->scale());
		gef::Sphere sphere_2(go_2->position(), go_2->mesh()->bounding_sphere().radius() * go_2->scale());

		// Calculate the squared distance between the centers of both spheres
		gef::Vector4 distance = sphere_2.position() - sphere_1.position();
		float distance_sq = distance.DotProduct(distance);

		// Calculate the squared sum of sphere radii
		float radii_sum = sphere_1.radius() + sphere_2.radius();
		float radii_sum_sq = radii_sum * radii_sum;

		// If distance squared is less than or equal to the square sum of the radii, spheres are colliding
		if (distance_sq <= radii_sum_sq)
		{
			// create a new collision
			Collision* new_collision = new Collision(go_1, go_2);
			// add to collision list
			collision_list_.push_back(new_collision);
		}
	}

	void Physics3D::BoundsCollision(GameObject* go_1, GameObject* go_2)
	{
		GameObject* bounds;
		GameObject* other;

		// check which object is the bounds
		if (go_1->go_type() == GameObject::BOUNDS)
		{
			bounds = go_1;
			other = go_2;
		}
		else
		{
			bounds = go_2;
			other = go_1;
		}

		// check whether near or far
		if (bounds->position().z() == NEAR_BOUNDS_Z)
		{
			// if other object z pos is equal or greater
			if (other->position().z() >= bounds->position().z())
			{
				// create a new collision
				Collision* new_collision = new Collision(go_1, go_2);
				// add to collision list
				collision_list_.push_back(new_collision);
			}
		}
		else
		{
			//  if other object z pos is equal or less
			if (other->position().z() <= bounds->position().z())
			{
				// create a new collision
				Collision* new_collision = new Collision(go_1, go_2);
				// add to collision list
				collision_list_.push_back(new_collision);
			}
		}
	
	}

	// return true if ray collides with the bounding sphere of 'game_object' 
	bool Physics3D::RayCastToSphere(Ray& ray, GameObject* game_object)
	{
		// Get sphere
		gef::Sphere sphere(game_object->position(), game_object->mesh()->bounding_sphere().radius() * game_object->scale());

		// Early out: Is ray starting inside sphere?
		if (PointInSphere(ray.origin, sphere))
			return true;

		// Get direction from the ray's origin to the sphere's center
		gef::Vector4 direction(sphere.position() - ray.origin);

		// Project onto the ray's direction vector
		float fD = direction.DotProduct(ray.direction);

		// Return false if the ray is pointing away
		if (fD < 0.0f)
			return false;

		// Otherwise calculate the closest point to the sphere
		gef::Vector4 closest_point(ray.origin + (ray.direction * fD));

		//Check if that point is inside the sphere
		return (PointInSphere(closest_point, sphere));

	}

	// return true if point falls within the bounding sphere 'sphere'
	bool Physics3D::PointInSphere(gef::Vector4 point, gef::Sphere& sphere)
	{
		//Calculate the squared distance from the point to the center of the sphere
		gef::Vector4 distance = sphere.position() - point;
		float distance_sq = distance.DotProduct(distance);

		//Return true if squared distance between the sphere's center and the point
		//is less than the squared radius of the sphere
		if (distance_sq < (sphere.radius() * sphere.radius()))
		{
			return true;
		}

		//If not, return false
		return false;
	}

	// return iterator to start of collision list
	std::list<Collision*>::const_iterator Physics3D::GetCollisionListStart()
	{
		return collision_list_.begin();
	}

	// return iterator to end of collision list
	std::list<Collision*>::const_iterator Physics3D::GetCollisionListEnd()
	{
		return collision_list_.end();
	}

	// free memory by emptying collisions list 
	void Physics3D::ClearCollisionList()
	{
		for (auto it = collision_list_.begin(); it != collision_list_.end();)
		{
			// store reference for deletion
			Collision* delete_collision = (*it);
			// erase from list
			it = collision_list_.erase(it);
			// delete
			delete delete_collision;
			delete_collision = NULL;
		}
	}
}