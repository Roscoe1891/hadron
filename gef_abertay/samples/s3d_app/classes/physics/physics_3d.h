#ifndef _PHYSICS_3D_H
#define _PHYSICS_3D_H

#include "collision.h"
#include <graphics/mesh.h>
#include <classes/game_object/bounds.h>
#include <list>

// Physics 3D is responsible for checking collisions
// Collisions are stored in a collision list
// They will remain there until ClearCollisionList is called

namespace s3d_app
{
#define NEAR_BOUNDS_Z 95
#define FAR_BOUNDS_Z -60

	// Define a ray used for raycasts
	struct Ray
	{
		gef::Vector4 direction;
		gef::Vector4 origin;
	};

	class Physics3D
	{
	public:
		Physics3D();
		~Physics3D();
		// Check for a collision between 2 game objects using collision detection specified by 'checktype'
		void CheckCollision(GameObject* go_1, GameObject* go_2, Collision::CollisionType checkType);
		// return true if ray collides with the bounding sphere of 'go_1' 
		bool RayCastToSphere(Ray& ray, GameObject* go_1);

		// return iterator to start of the collision list
		std::list<Collision*>::const_iterator GetCollisionListStart();
		// return iterator to end of the collision list
		std::list<Collision*>::const_iterator GetCollisionListEnd();
		// clear the collision list
		void ClearCollisionList();

	private:
		// carry out collision detection based on 2 spheres
		void SphereCollision(GameObject* go_1, GameObject* go_2);
		// carry out collision detection where 'go_1' or 'go_2' is of type 'BOUNDS'
		void BoundsCollision(GameObject* go_1, GameObject* go_2);
		// return true if point falls within the bounding sphere 'sphere'
		bool PointInSphere(gef::Vector4 point, gef::Sphere& sphere);

		// The Collision list
		std::list<Collision*> collision_list_;
	};
}

#endif