#include <platform/d3d11/system/platform_d3d11.h>
#include "s3d_app.h"


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	// initialisation
	gef::PlatformD3D11 platform(hInstance, 960, 544, false, true);

	// create and run app
	S3DApp myApp(platform);
	myApp.Run();

	return 0;
}