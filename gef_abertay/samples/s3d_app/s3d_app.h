#ifndef _RENDER_TARGET_APP_H
#define _RENDER_TARGET_APP_H

#include <system/application.h>
#include <graphics/sprite.h>
#include <maths/vector2.h>
#include <vector>
#include <graphics/mesh_instance.h>
#include <classes/states/state_manager.h>
#include <input/input_manager.h>
#include <classes/graphics/camera.h>
#include <classes/graphics/mesh_builder.h>
#include <classes/graphics/mesh_data_manager.h>
#include <classes/graphics/s3d_renderer.h>

// FRAMEWORK FORWARD DECLARATIONS
namespace gef
{
	class Platform;
	class SpriteRenderer;
	class Font;
	class Mesh;
	class Scene;
	class Shader;
}

/// Application-derived class for an S3D app

class S3DApp : public gef::Application
{
public:
	S3DApp(gef::Platform& platform);
	// Init App
	void Init();
	// Free memory
	void CleanUp();
	// Update app
	bool Update(float frame_time);
	// Render
	void Render();
private:
	// Initialise font
	void InitFont();
	// Free memory used by font
	void CleanUpFont();
	// Render HUD / UI objects
	void DrawHUD();
	// Set light values
	void SetupLights();

	// 2D sprite renderer
	gef::SpriteRenderer* sprite_renderer_;
	// font for game text
	gef::Font* font_;

	// current frame rate
	float fps_;

	// Input manager
	gef::InputManager* input_manager_;
	
	// Stereoscopic 3D renderer
	s3d_app::S3DRenderer s3d_renderer_;
	
	// Camera
	s3d_app::Camera camera_;
	
	// State Manager
	s3d_app::StateManager state_manager_;

	// Game Data
	s3d_app::GameData game_data_;

	// Mesh Data Manager
	s3d_app::MeshDataManager mesh_data_manager_;
};

#endif // _RENDER_TARGET_APP_H