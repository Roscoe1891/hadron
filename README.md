# README #

This application was built using the Games Education Framework, developed by Grant Clarke.
It is available separately at : https://github.com/grantclarke-abertay/gef.git

On downloading the full source code from this repo, the Hadron-specific source files can be found in gef_abertay\samples\s3d_app.